// SPDX-FileCopyrightText: 2023 Marigold <contact@marigold.dev>
//
// SPDX-License-Identifier: MIT

// depedency: npm install ethereumjs-wallet --save
// usage: node make_addr.js

var Wallet = require('ethereumjs-wallet');
const EthWallet = Wallet.default.generate();
console.log("address: " + EthWallet.getAddressString());
console.log("privateKey: " + EthWallet.getPrivateKeyString());
console.log("publicKey: " + EthWallet.getPublicKeyString());