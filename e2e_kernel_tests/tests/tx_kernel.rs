// SPDX-FileCopyrightText: 2022-2023 TriliTech <contact@trili.tech>
// SPDX-FileCopyrightText: 2023 Marigold <contact@marigold.dev>
// SPDX-FileCopyrightText: 2022 Nomadic Labs <contact@nomadic-labs.com>
//
// SPDX-License-Identifier: MIT

//! Tests covering the top-level behaviour of the *Transactions Kernel*.

use crypto::hash::{ContractKt1Hash, HashTrait, SmartRollupHash};
use host::metadata::RollupMetadata;
use kernel::kernel_entry;
use kernel_core::{
    bls::BlsKey,
    encoding::{
        contract::Contract,
        entrypoint::Entrypoint,
        michelson::{MichelsonPair, MichelsonString},
        public_key_hash::PublicKeyHash,
        smart_rollup::SmartRollupAddress,
        string_ticket::{StringTicket, StringTicketRepr},
    },
    inbox::{
        sendable::ExternalInboxMessage,
        sendable::InboxMessage,
        v1::{
            sendable::{Batch, Transaction},
            Operation, OperationContent,
        },
        InternalInboxMessage, Signer, Transfer,
    },
    outbox::{OutboxMessage, OutboxMessageTransaction},
    storage::{account_path, init_account_storage},
    transactions_run,
};
use mock_host::{host_loop, HostInput};
use mock_runtime::{host::MockHost, state::HostState};
use tezos_encoding::{enc::BinWriter, nom::NomReader};

#[test]
fn tx_deposit_then_withdraw_to_same_address() {
    kernel_entry!(transactions_run);
    let init = HostState::default();

    // Arrange
    let bls_key = BlsKey::from_ikm([1; 32]);
    let signer = Signer::BlsPublicKey(bls_key.compressed_public_key().clone());
    let address = bls_key.public_key_hash().clone();

    let originator = Contract::Originated(
        ContractKt1Hash::from_b58check("KT1ThEdxfUcWUwqsdergy3QnbCWGHSUHeHJq").unwrap(),
    );
    let contents = "Hello, Ticket!".to_string();

    let string_ticket = StringTicket::new(originator.clone(), contents.clone(), 500);

    let sender =
        ContractKt1Hash::from_b58check("KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn").unwrap();

    let source =
        PublicKeyHash::from_b58check("tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU").unwrap();

    let destination = SmartRollupAddress::new(init.get_metadata().address().unwrap());

    // Deposit
    let deposit = InboxMessage::Internal(InternalInboxMessage::Transfer(Transfer {
        payload: MichelsonPair(
            MichelsonString(address.to_b58check()),
            string_ticket.into(),
        ),
        sender: sender.clone(),
        source,
        destination: destination.clone(),
    }));

    let mut deposit_message = Vec::new();
    deposit.bin_write(&mut deposit_message).unwrap();

    // Withdrawal
    let string_ticket = StringTicket::new(
        Contract::Originated(
            ContractKt1Hash::from_b58check("KT1ThEdxfUcWUwqsdergy3QnbCWGHSUHeHJq")
                .unwrap(),
        ),
        "Hello, Ticket!".to_string(),
        450,
    );
    let withdrawal = OperationContent::withdrawal(
        sender.clone(),
        string_ticket,
        Entrypoint::default(),
    );
    let operation = Operation {
        signer,
        counter: 0,
        contents: vec![withdrawal],
    };
    let transaction =
        Transaction::new([(operation, bls_key.clone())]).expect("Valid Transaction");
    let batch = Batch::new(vec![transaction]);
    let message =
        InboxMessage::<MichelsonPair<MichelsonString, StringTicketRepr>>::External(
            ExternalInboxMessage::V1(batch),
        );
    let mut withdrawal_message = Vec::new();

    message.bin_write(&mut withdrawal_message).unwrap();

    // Act

    let host_next = |level: i32| -> HostInput {
        if level > 1 {
            HostInput::Exit
        } else {
            HostInput::NextLevel(1)
        }
    };

    let input_messages = |level: i32| -> Vec<Vec<u8>> {
        if level == 1 {
            vec![deposit_message.clone(), withdrawal_message.clone()]
        } else {
            vec![]
        }
    };

    let final_state = host_loop(init, mock_kernel_run, host_next, input_messages);

    // Assert: withdrawal outbox message at level one
    let outputs: Vec<_> = final_state
        .store
        .as_ref()
        .iter()
        .filter(|(k, _)| k.starts_with("/output") && k.as_str() != "/output/id")
        .collect();

    assert_eq!(1, outputs.len(), "There should be a single outbox message");

    let path = outputs[0].0;
    assert_eq!("/output/1/0", path);
    let outbox_message = outputs[0].1;

    let (remaining, outbox_message) = OutboxMessage::nom_read(outbox_message.as_slice())
        .expect("Parsing outbox message should work");

    assert!(
        remaining.is_empty(),
        "Unexpected excess bytes in outbox message"
    );

    let transactions = match outbox_message {
        OutboxMessage::AtomicTransactionBatch(transactions) => transactions,
    };
    assert_eq!(1, transactions.len(), "Expected single outbox transaction");

    let string_ticket = StringTicket::new(
        Contract::Originated(
            ContractKt1Hash::from_b58check("KT1ThEdxfUcWUwqsdergy3QnbCWGHSUHeHJq")
                .unwrap(),
        ),
        "Hello, Ticket!".to_string(),
        450,
    );
    let expected = OutboxMessageTransaction {
        parameters: string_ticket.into(),
        destination: Contract::Originated(sender),
        entrypoint: Entrypoint::default(),
    };

    assert_eq!(expected, transactions[0]);
}

// Test coverring:
// - ability to withdraw tickets in an account
// - ability to transfer tickets between accounts, as part of a transaction
// - transactions should fail when there is insufficient balance to perform an operation
#[test]
fn tx_deposit_transfer_withdraw() {
    kernel_entry!(transactions_run);

    // Arrange

    // signer representing first account
    let rollup_destination = "sr1VHXUw1hMueFzWKYnoXYByFiKxQz4BRpGm";
    let fst_bls_key = BlsKey::from_ikm([1; 32]);
    let fst_signer_key =
        Signer::BlsPublicKey(fst_bls_key.compressed_public_key().clone());
    let fst_address = fst_bls_key.public_key_hash().clone();

    // signer representing second account
    let snd_bls_key = BlsKey::from_ikm([2; 32]);
    let snd_signer_key =
        Signer::BlsPublicKey(snd_bls_key.compressed_public_key().clone());
    let snd_address = snd_bls_key.public_key_hash().clone();

    let originator = Contract::Originated(
        ContractKt1Hash::from_b58check("KT1ThEdxfUcWUwqsdergy3QnbCWGHSUHeHJq").unwrap(),
    );

    // A ticket that will be deposited into account 1, and transferred to account 2
    let make_fst_ticket = |amount: u64| {
        StringTicket::new(originator.clone(), "Hello, ticket".to_string(), amount)
    };
    // A ticket that will be deposited into account 2, and transferred to account 1
    let make_snd_ticket = |amount: u64| {
        StringTicket::new(originator.clone(), "Goodbye, ticket".to_string(), amount)
    };

    let sender =
        ContractKt1Hash::from_b58check("KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn").unwrap();

    let source =
        PublicKeyHash::from_b58check("tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU").unwrap();

    let destination = SmartRollupAddress::from_b58check(rollup_destination).unwrap();

    // Deposit

    // deposit first ticket into account 1
    let fst_deposit = InboxMessage::Internal(InternalInboxMessage::Transfer(Transfer {
        payload: MichelsonPair(
            MichelsonString(fst_address.to_b58check()),
            make_fst_ticket(300).into(),
        ),
        sender: sender.clone(),
        source: source.clone(),
        destination: destination.clone(),
    }));

    let mut fst_deposit_message = Vec::new();
    fst_deposit.bin_write(&mut fst_deposit_message).unwrap();

    // deposit second ticket into account 2
    let snd_deposit = InboxMessage::Internal(InternalInboxMessage::Transfer(Transfer {
        payload: MichelsonPair(
            MichelsonString(snd_address.to_b58check()),
            make_snd_ticket(300).into(),
        ),
        sender: sender.clone(),
        source: source.clone(),
        destination: destination.clone(),
    }));

    let mut snd_deposit_message = Vec::new();
    snd_deposit.bin_write(&mut snd_deposit_message).unwrap();

    // Withdrawal

    // withdraw 100 of the first ticket
    let fst_withdrawal = || {
        OperationContent::withdrawal(
            sender.clone(),
            make_fst_ticket(100),
            Entrypoint::default(),
        )
    };
    // withdraw 98 of the second ticket
    let snd_withdrawal = || {
        OperationContent::withdrawal(
            sender.clone(),
            make_snd_ticket(98),
            Entrypoint::default(),
        )
    };

    // Transfer
    let snd_to_fst_account_transfer =
        || OperationContent::transfer(fst_address.clone(), make_snd_ticket(99));
    let fst_to_snd_account_transfer =
        || OperationContent::transfer(snd_address.clone(), make_fst_ticket(150));

    // Invalid, as account 2 has not transferred the 'snd_ticket' to account 1
    // before account 1 withdraws.
    let invalid_transaction = Transaction::new([
        (
            Operation {
                signer: fst_signer_key.clone(),
                counter: 0,
                contents: vec![
                    fst_to_snd_account_transfer(),
                    fst_withdrawal(),
                    snd_withdrawal(),
                ],
            },
            fst_bls_key.clone(),
        ),
        (
            Operation {
                signer: snd_signer_key.clone(),
                counter: 0,
                contents: vec![
                    snd_to_fst_account_transfer(),
                    fst_withdrawal(),
                    snd_withdrawal(),
                ],
            },
            snd_bls_key.clone(),
        ),
    ])
    .unwrap();

    let valid_transaction = Transaction::new([
        (
            Operation {
                signer: fst_signer_key.clone(),
                counter: 0,
                contents: vec![fst_to_snd_account_transfer(), fst_withdrawal()],
            },
            fst_bls_key.clone(),
        ),
        (
            Operation {
                signer: snd_signer_key.clone(),
                counter: 0,
                contents: vec![snd_to_fst_account_transfer(), snd_withdrawal()],
            },
            snd_bls_key.clone(),
        ),
    ])
    .unwrap();

    let valid_final_withdraw = Transaction::new([
        (
            Operation {
                signer: Signer::Layer2Address(fst_address.clone()), // bls key now in cache
                counter: 1,
                contents: vec![snd_withdrawal()],
            },
            fst_bls_key.clone(),
        ),
        (
            Operation {
                signer: Signer::Layer2Address(snd_address.clone()), // bls key now in cache
                counter: 1,
                contents: vec![fst_withdrawal()],
            },
            snd_bls_key.clone(),
        ),
    ])
    .unwrap();

    // Act
    let init = HostState::default_with_metadata(RollupMetadata {
        raw_rollup_address: SmartRollupHash::from_b58check(rollup_destination)
            .unwrap()
            .0
            .try_into()
            .unwrap(),
        origination_level: 1,
    });
    let invalid_batch = Batch::new(vec![invalid_transaction]);
    let invalid_message =
        InboxMessage::<MichelsonPair<MichelsonString, StringTicketRepr>>::External(
            ExternalInboxMessage::V1(invalid_batch),
        );
    let mut invalid_external_message = Vec::new();
    invalid_message
        .bin_write(&mut invalid_external_message)
        .unwrap();

    let valid_batch = Batch::new(vec![valid_transaction, valid_final_withdraw]);
    let valid_message =
        InboxMessage::<MichelsonPair<MichelsonString, StringTicketRepr>>::External(
            ExternalInboxMessage::V1(valid_batch),
        );
    let mut valid_external_message = Vec::new();
    valid_message
        .bin_write(&mut valid_external_message)
        .unwrap();

    let host_next = |level: i32| -> HostInput {
        if level > 1 {
            HostInput::Exit
        } else {
            HostInput::NextLevel(1)
        }
    };

    let input_messages = |level: i32| -> Vec<Vec<u8>> {
        if level == 1 {
            vec![
                fst_deposit_message.clone(),
                invalid_external_message.clone(),
                snd_deposit_message.clone(),
                valid_external_message.clone(),
            ]
        } else {
            vec![]
        }
    };

    let final_state = host_loop(init, mock_kernel_run, host_next, input_messages);

    // Assert: withdrawal outbox message at level one
    let mut outputs: Vec<_> = final_state
        .store
        .as_ref()
        .iter()
        .filter(|(k, _)| k.starts_with("/output") && k.as_str() != "/output/id")
        .collect();
    outputs.sort();

    assert_eq!(2, outputs.len(), "There should be two outbox messages");

    // First withdrawal:
    // - withdraw ticket 1 from account 1
    // - withdraw ticket 2 from account 2
    let path = outputs[0].0;
    assert_eq!("/output/1/0", path);
    let outbox_message = outputs[0].1;

    let (remaining, outbox_message) = OutboxMessage::nom_read(outbox_message.as_slice())
        .expect("Parsing outbox message should work");
    assert!(
        remaining.is_empty(),
        "Unexpected excess bytes in outbox message"
    );

    let transactions = match outbox_message {
        OutboxMessage::AtomicTransactionBatch(transactions) => transactions,
    };
    assert_eq!(2, transactions.len(), "Expected two outbox transactions");

    let fst_expected = OutboxMessageTransaction {
        parameters: make_fst_ticket(100).into(),
        destination: Contract::Originated(sender.clone()),
        entrypoint: Entrypoint::default(),
    };

    let snd_expected = OutboxMessageTransaction {
        parameters: make_snd_ticket(98).into(),
        destination: Contract::Originated(sender.clone()),
        entrypoint: Entrypoint::default(),
    };

    assert_eq!(fst_expected, transactions[0]);
    assert_eq!(snd_expected, transactions[1]);

    // Second withdrawal:
    // - withdraw ticket 2 from account 1
    // - withdraw ticket 1 from account 2
    let path = outputs[1].0;
    assert_eq!("/output/1/1", path);
    let outbox_message = outputs[1].1;

    let (remaining, outbox_message) = OutboxMessage::nom_read(outbox_message.as_slice())
        .expect("Parsing outbox message should work");
    assert!(
        remaining.is_empty(),
        "Unexpected excess bytes in outbox message"
    );

    let transactions = match outbox_message {
        OutboxMessage::AtomicTransactionBatch(transactions) => transactions,
    };
    assert_eq!(2, transactions.len(), "Expected two outbox transactions");

    let fst_expected = OutboxMessageTransaction {
        parameters: make_snd_ticket(98).into(),
        destination: Contract::Originated(sender.clone()),
        entrypoint: Entrypoint::default(),
    };

    let snd_expected = OutboxMessageTransaction {
        parameters: make_fst_ticket(100).into(),
        destination: Contract::Originated(sender.clone()),
        entrypoint: Entrypoint::default(),
    };

    assert_eq!(fst_expected, transactions[0]);
    assert_eq!(snd_expected, transactions[1]);
}

#[test]
fn tx_filter_internal_transfers_not_addressed_to_rollup() {
    kernel_entry!(transactions_run);

    // setup rollup state
    let init = HostState::default();

    // setup message
    let bls_key = BlsKey::from_ikm([1; 32]);
    let receiver = bls_key.public_key_hash().clone();
    let originator = Contract::Originated(
        ContractKt1Hash::from_b58check("KT1ThEdxfUcWUwqsdergy3QnbCWGHSUHeHJq").unwrap(),
    );
    let contents = "Hello, Ticket!".to_string();
    let string_ticket = StringTicket::new(originator.clone(), contents.clone(), 500);
    let sender =
        ContractKt1Hash::from_b58check("KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn").unwrap();
    let source =
        PublicKeyHash::from_b58check("tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU").unwrap();
    // Transfers
    // ignore transfer 100
    let transfer1 = InboxMessage::Internal(InternalInboxMessage::Transfer(Transfer {
        payload: MichelsonPair(
            MichelsonString(receiver.to_b58check()),
            StringTicket::new(originator.clone(), contents.clone(), 100).into(),
        ),
        sender: sender.clone(),
        source: source.clone(),
        destination: SmartRollupAddress::from_b58check(
            "sr1Uz7zvuKLjWdK2SfnA5Axxvxfq94PoZNKz",
        )
        .unwrap(),
    }));
    let mut message1 = vec![];
    transfer1.bin_write(&mut message1).unwrap();

    // apply transfer 500
    let transfer2 = InboxMessage::Internal(InternalInboxMessage::Transfer(Transfer {
        payload: MichelsonPair(
            MichelsonString(receiver.to_b58check()),
            string_ticket.testing_clone().into(),
        ),
        sender: sender.clone(),
        source,
        destination: SmartRollupAddress::new(init.get_metadata().address().unwrap()),
    }));
    let mut message2 = vec![];
    transfer2.bin_write(&mut message2).unwrap();

    let messages = vec![message1, message2];

    let host_next = |level: i32| -> HostInput {
        if level > 2 {
            HostInput::Exit
        } else {
            HostInput::NextLevel(level + 1)
        }
    };

    let input_messages = |level: i32| -> Vec<Vec<u8>> {
        if level == 2 {
            messages.clone()
        } else {
            vec![]
        }
    };

    let final_state = host_loop(init, mock_kernel_run, host_next, input_messages);

    let mock_runtime = MockHost::from(final_state);
    let account_storage = init_account_storage().unwrap();
    let receiver_account = account_storage
        .get_account(&mock_runtime, &account_path(&receiver).unwrap())
        .unwrap()
        .unwrap();
    let ticket_amount = receiver_account
        .ticket_amount(&mock_runtime, &string_ticket.identify().unwrap())
        .unwrap()
        .unwrap();
    assert_eq!(500, ticket_amount);
}
