// SPDX-FileCopyrightText: 2022 TriliTech <contact@trili.tech>
// SPDX-FileCopyrightText: 2023 Marigold <contact@marigold.dev>
//
// SPDX-License-Identifier: MIT

#[macro_use]
extern crate kernel;
#[macro_use]
extern crate debug;
extern crate alloc;

use host::{
    input::Message,
    rollup_core::{RawRollupCore, MAX_INPUT_MESSAGE_SIZE},
    runtime::Runtime,
};
use mock_host::{host_loop, HostInput};
use mock_runtime::state::HostState;

fn io_kernel<Host: RawRollupCore>(host: &mut Host) {
    match host.read_input(MAX_INPUT_MESSAGE_SIZE) {
        Ok(Some(input @ Message { level, id, .. })) => {
            debug_msg!(
                Host,
                "message input recieved at -level:{} -id:{}",
                level,
                id
            );
            host.write_output(input.as_ref()).unwrap();
        }
        Ok(None) => debug_msg!(Host, "no input recieved"),
        Err(_) => todo!("Handle errors in io_kernel"),
    }
}

kernel_entry!(io_kernel);

fn host_next(level: i32) -> HostInput {
    if level < 5 {
        HostInput::NextLevel(level)
    } else {
        HostInput::Exit
    }
}

fn get_input_batch(level: i32) -> Vec<Vec<u8>> {
    (1..level)
        .map(|l| {
            let bytes = format!("message at {} value {}", level, l).into();
            bytes
        })
        .collect()
}

#[test]
fn test() {
    // Arrange
    let init = HostState::default();

    // Act
    let final_state = host_loop(init, mock_kernel_run, host_next, get_input_batch);

    // Assert inputs have been written to outputs
    let mut outputs: Vec<_> = final_state
        .store
        .as_ref()
        .iter()
        .filter(|(k, _)| k.starts_with("/output") && k.as_str() != "/output/id")
        .collect();
    outputs.sort();

    let mut inputs: Vec<_> = final_state
        .store
        .as_ref()
        .iter()
        .filter(|(k, _)| k.starts_with("/input") && k.contains("/payload"))
        .collect();
    inputs.sort();

    assert_eq!(
        outputs.iter().map(|(_, v)| v).collect::<Vec<_>>(),
        inputs.iter().map(|(_, v)| v).collect::<Vec<_>>()
    );
}
