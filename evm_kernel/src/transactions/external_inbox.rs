// SPDX-FileCopyrightText: 2022-2023 TriliTech <contact@trili.tech>
//
// SPDX-License-Identifier: MIT

//! Processing external inbox messages - withdrawals & transactions.

use super::TransactionError;
use super::{begin_transaction, commit_transaction, rollback_transaction};
use crate::ethereum::block::BlockConstants;
use crate::ethereum::call_evm;
use crate::ethereum::precompiles::PrecompileBTreeMap;
use crate::ethereum::signatures::EthereumTransaction;
use crate::inbox::v1::{Transaction, TransactionBatch};
use crate::inbox::{ExternalInboxMessage, SerializedExternalInboxMessage};
use debug::debug_msg;
use host::rollup_core::RawRollupCore;
use kernel_core::inbox::external::v1::verifiable::VerifiableTransaction as VerifiableTicketTransaction;
use kernel_core::storage::AccountStorage as TxAccountStorage;
use kernel_core::transactions::withdrawal::Withdrawal;
/// Process external message
///
/// An external message is a batch of transactions. Each transaction
/// may be either a series of signed operations or a single Ethereum
/// transaction.
pub fn process_external<'a, Host: RawRollupCore>(
    host: &mut Host,
    tx_account_storage: &mut TxAccountStorage,
    block: &'a BlockConstants,
    precompiles: &'a PrecompileBTreeMap<Host>,
    message: SerializedExternalInboxMessage<'a>,
) -> Vec<Vec<Withdrawal>> {
    let batch = match parse_external::<Host>(message) {
        Some(batch) => batch,
        None => return vec![],
    };

    let mut all_withdrawals: Vec<Vec<Withdrawal>> = Vec::new();

    if let Err(e) = batch.verify_signature(host, tx_account_storage) {
        debug_msg!(Host, "Signature verification of batch failed: {}", e);
    } else {
        for transaction in batch.transactions.into_iter() {
            if begin_transaction(host, tx_account_storage).is_err() {
                panic!("Could not begin a transaction");
            }

            match transaction {
                Transaction::EthereumTransaction(ethereum_transaction) => {
                    match process_ethereum_transaction(
                        host,
                        tx_account_storage,
                        block,
                        precompiles,
                        ethereum_transaction,
                    ) {
                        Ok(_) => {
                            if commit_transaction(host, tx_account_storage).is_err() {
                                panic!("Failed to commit transaction");
                            }
                        }
                        Err(err) => {
                            debug_msg!(
                                Host,
                                "Could not execute Ethereum transaction: {}",
                                err
                            );

                            if rollback_transaction(host, tx_account_storage).is_err() {
                                panic!("Failed to rollback transaction");
                            }
                        }
                    }
                }
                Transaction::TicketTransaction(ticket_transaction) => {
                    match process_ticket_transaction(
                        host,
                        tx_account_storage,
                        ticket_transaction,
                    ) {
                        Ok(withdrawals) => {
                            if commit_transaction(host, tx_account_storage).is_err() {
                                panic!("Failed to commit transaction");
                            }

                            all_withdrawals.push(withdrawals);
                        }
                        Err(err) => {
                            debug_msg!(Host, "Could not execute transaction: {}", err);

                            if rollback_transaction(host, tx_account_storage).is_err() {
                                panic!("Failed to rollback transaction");
                            }
                        }
                    }
                }
            }
        }
    }

    all_withdrawals
}

/// Process one Ethereum transaction
fn process_ethereum_transaction<'a, Host: RawRollupCore>(
    host: &mut Host,
    _tx_account_storage: &mut TxAccountStorage,
    block: &'a BlockConstants,
    precompiles: &'a PrecompileBTreeMap<Host>,
    message: EthereumTransaction,
) -> Result<(), TransactionError> {
    match message {
        EthereumTransaction::Classical(e) => call_evm(
            host,
            block,
            precompiles,
            e.to.into(),
            e.caller().into(),
            &e.data,
        )
        .map_err(TransactionError::from),
        EthereumTransaction::EIP1559(e) => call_evm(
            host,
            block,
            precompiles,
            e.to.into(),
            e.caller().into(),
            &e.data,
        )
        .map_err(TransactionError::from),
    }
}

/// Process one TX kernel transaction
fn process_ticket_transaction<Host: RawRollupCore>(
    host: &mut Host,
    tx_account_storage: &mut TxAccountStorage,
    transaction: VerifiableTicketTransaction,
) -> Result<Vec<Withdrawal>, TransactionError> {
    transaction
        .execute(host, tx_account_storage)
        .map_err(TransactionError::from)
}

/// Parse external message, logging error if it occurs.
fn parse_external<Host: RawRollupCore>(
    message: SerializedExternalInboxMessage,
) -> Option<TransactionBatch> {
    match ExternalInboxMessage::parse(message.0) {
        Ok((remaining, ExternalInboxMessage::V1(batch))) => {
            if !remaining.is_empty() {
                debug_msg!(
                    Host,
                    "External message had unused remaining bytes: {:?}",
                    remaining
                );
            }
            Some(batch)
        }
        Err(err) => {
            debug_msg!(Host, "Error parsing external message payload {}", err);
            None
        }
    }
}
