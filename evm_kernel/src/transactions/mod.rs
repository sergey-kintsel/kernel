// SPDX-FileCopyrightText: 2022-2023 TriliTech <contact@trili.tech>
//
// SPDX-License-Identifier: MIT

//! Handle transactions
//!
//! Handle both regular ticket transactions and also EVM transactions, ie,
//! contract creation, calling contracts, and eth transfers while emulating
//! the functionality of Ethereum.

use crate::ethereum::EthereumError;
use debug::debug_msg;
use host::rollup_core::RawRollupCore;
use kernel_core::inbox::external::v1::verifiable::TransactionError as TicketTransactionError;
use kernel_core::storage::AccountStorage as TxAccountStorage;
use tezos_rollup_storage::StorageError;
use thiserror::Error;

pub mod external_inbox;

/// Errors when processing transactions
#[derive(Error, Debug)]
pub enum TransactionError {
    /// Something went wrong when using the durable storage for transactions
    #[error("Error when using durable storage for transactions: {0}")]
    TransactionStorageError(#[from] StorageError),
    /// An Ethereum transaction failed - either call, create or transfer
    #[error("Error when processing an Ethereum transaction: {0}")]
    EthereumTransactionError(#[from] EthereumError),
    /// A ticket transaction failed
    #[error("Error when processing a ticket transaction: {0}")]
    TicketTransactionError(#[from] TicketTransactionError),
}

/// Begin a transaction for both ticket transactions _and_
/// Ethereum transactions (when we have an implementation for
/// Ethereum transactions).
fn begin_transaction<Host: RawRollupCore>(
    host: &mut Host,
    tx_account_storage: &mut TxAccountStorage,
) -> Result<(), TransactionError> {
    debug_msg!(Host, "Begin transaction");
    tx_account_storage
        .begin_transaction(host)
        .map_err(TransactionError::from)
}

/// Commit a transaction in durable storage.
fn commit_transaction<Host: RawRollupCore>(
    host: &mut Host,
    tx_account_storage: &mut TxAccountStorage,
) -> Result<(), TransactionError> {
    debug_msg!(Host, "Commit transaction");
    tx_account_storage
        .commit(host)
        .map_err(TransactionError::from)
}

/// Rollback a transaction in durable storage.
fn rollback_transaction<Host: RawRollupCore>(
    host: &mut Host,
    tx_account_storage: &mut TxAccountStorage,
) -> Result<(), TransactionError> {
    debug_msg!(Host, "Rollback transaction");
    tx_account_storage
        .rollback(host)
        .map_err(TransactionError::from)
}
