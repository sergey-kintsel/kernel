// SPDX-FileCopyrightText: 2022-2023 TriliTech <contact@trili.tech>
//
// SPDX-License-Identifier: MIT

//! Construct messages for the EVM kernel
//!
//! This is an expanded version of same functionality for the Transaction
//! Kernel, and most code has been adapted from there. The only change is
//! adding EVM related transactions to the "external" kind of messages.

use crate::ethereum::signatures::EthereumTransaction;
use crate::inbox::v1::CompressedSignature;
use kernel_core::bls::BlsKey;
use kernel_core::bls::Signature;
use kernel_core::encoding::michelson::Michelson;
use kernel_core::inbox::external::v1::Operation;
use kernel_core::inbox::external::v1::ToBytesError;
use tezos_encoding::enc::{self, BinError, BinErrorKind, BinWriter};
use tezos_encoding::encoding::{Encoding, HasEncoding};
use tezos_encoding::has_encoding;

pub use kernel_core::inbox::external::v1::sendable::Transaction as SendableTxTransaction;
pub use kernel_core::inbox::sendable::InternalInboxMessage;

/// EVM inbox messaages, external messages include EVM transactions.
#[derive(Debug, PartialEq, HasEncoding, BinWriter)]
pub enum InboxMessage<Expr: Michelson> {
    /// Message sent from an L1 smart-contract.
    Internal(InternalInboxMessage<Expr>),
    /// Message of arbitrary bytes, in a format specific to the kernel.
    /// These bytes encode messages accepted by the transaction kernel,
    /// with only slight modification (an additional prefixed byte to
    /// identify transaction type), and EVM calls.
    External(ExternalInboxMessage),
}

/// Messages from L1 implicit accounts. Almost the same as for TX kernel,
/// but includes EVM transactions.
#[derive(Debug, PartialEq, HasEncoding, BinWriter)]
pub enum ExternalInboxMessage {
    /// Version 1 of external inbox messages for the EVM kernel
    V1(SendableBatch),
}

/// Sendable batch of transactions
#[derive(Debug, PartialEq)]
pub struct SendableBatch {
    /// All transactions in the batch. Mix of tx kernel and Ethereum
    /// transactions.
    pub transactions: Vec<SendableTransaction>,
}

has_encoding!(SendableBatch, SENDABLE_BATCH_ENCODING, { Encoding::Custom });

impl SendableBatch {
    /// Create a new batch from transactions. Transactions can be both Tx kernel
    /// transactions and Ethereum transactions. Adapted from TX kernel.
    pub fn new(transactions: Vec<SendableTransaction>) -> Self {
        Self { transactions }
    }

    /// Write output to vector passed as argument and return the signature
    fn to_bytes_signed(
        &self,
        output: &mut Vec<u8>,
    ) -> Result<Option<CompressedSignature>, ToBytesError> {
        let signed_bytes = self
            .transactions
            .iter()
            .map(|t| t.to_bytes_signed())
            .collect::<Result<Vec<(_, _)>, _>>()?;

        let signatures = signed_bytes
            .iter()
            .filter_map(|(sig, _)| sig.as_ref())
            .collect::<Vec<_>>();
        let compressed_sig = if !signatures.is_empty() {
            let sig = Signature::aggregate_sigs(signatures.as_slice())?;
            Some(CompressedSignature::from(&sig))
        } else {
            None
        };

        let output_bytes = signed_bytes
            .into_iter()
            .flat_map(|(_, bytes)| bytes)
            .collect::<Vec<u8>>();

        enc::dynamic(|output_bytes: Vec<u8>, output: &mut Vec<u8>| {
            output.extend(output_bytes);
            Ok(())
        })(output_bytes, output)?;

        Ok(compressed_sig)
    }
}

impl BinWriter for SendableBatch {
    /// Serialize a SendableBatch of transactions including the signature. Adapted from the
    /// transactions kernel.
    fn bin_write(&self, output: &mut Vec<u8>) -> enc::BinResult {
        let sig = self
            .to_bytes_signed(output)
            .map_err(|e| BinError::from(BinErrorKind::CustomError(format!("{}", e))))?;

        match sig {
            None => {
                output.push(0u8);
                Ok(())
            }
            Some(compressed_sig) => {
                output.push(1u8);
                compressed_sig.bin_write(output)
            }
        }
    }
}

/// Transactions for the EVM kernel. Includes TX kernel kind of transactions
/// (sequences of operations), and EVM transactions: calls, creates, and transfers
#[derive(Debug, PartialEq, HasEncoding, BinWriter)]
#[allow(clippy::large_enum_variant)]
pub enum SendableTransaction {
    /// Tx kernel transactions.
    TxTransaction(SendableTxTransaction),
    /// Ethereum transactions.
    EthereumTransaction(EthereumTransaction),
}

impl SendableTransaction {
    /// Create a new transactions kernel transaction.
    pub fn new_ticket_transaction(
        ops_with_keys: impl IntoIterator<Item = (Operation, BlsKey)>,
    ) -> Result<Self, ToBytesError> {
        let inner = SendableTxTransaction::new(ops_with_keys)?;
        Ok(SendableTransaction::TxTransaction(inner))
    }

    /// Create a new transaction for calling an Ethereum contract.
    pub fn new_evm_call(transaction: EthereumTransaction) -> Result<Self, ToBytesError> {
        Ok(SendableTransaction::EthereumTransaction(transaction))
    }

    /// Serialize a single transaction
    pub fn to_bytes_signed(&self) -> Result<(Option<Signature>, Vec<u8>), ToBytesError> {
        match self {
            SendableTransaction::TxTransaction(t) => {
                let (sig, msg) = t.to_bytes_signed()?;
                let mut output = vec![0_u8];
                output.extend(msg);
                Ok((Some(sig), output))
            }
            SendableTransaction::EthereumTransaction(t) => {
                let mut output = vec![1_u8];
                t.bin_write(&mut output)?;
                Ok((None, output))
            }
        }
    }
}
#[cfg(test)]
mod test {

    use proptest::collection;
    use proptest::prelude::*;
    use tezos_encoding::enc::BinWriter;

    use crate::inbox::v1::sendable::{SendableBatch, SendableTransaction};
    use crate::inbox::v1::{EthereumTransaction, Transaction, TransactionBatch};
    use kernel_core::inbox::external::v1::Operation;

    proptest! {
        #[test]
        fn sendable_evm_transaction_encode_decode(
            common in EthereumTransaction::arb(),
            remaining_input in any::<Vec<u8>>(),
        ) {
            // Arrange
            let transaction = SendableTransaction::new_evm_call(common.clone()).unwrap();
            let mut encoded = Vec::new();
            transaction.bin_write(&mut encoded).unwrap();

            encoded.extend_from_slice(remaining_input.as_slice());

            let (remaining, parsed_transaction) = Transaction::parse(encoded.as_slice())
                .expect("Parsing of encoded evm call transaction failed");

            // Assert
            if let Transaction::EthereumTransaction(t) = parsed_transaction {
                assert_eq!(common, t, "Common data part of transaction not parsed correctly");
                //  assert_eq!(input, t.data, "Input data to Etherem call not parsed correctly");
            } else {
                assert!(false, "Ethereum transaction has wrong type - not a call");
            }

            assert_eq!(remaining_input, remaining, "Parsing evm call transaction consumed too many bytes");
        }

        #[test]
        fn evm_transaction_non_signature(
            common in EthereumTransaction::arb(),

        ) {
            // Arrange
            let transaction = SendableTransaction::new_evm_call(common.clone()).unwrap();

            let (signature, transaction_bytes) = transaction.to_bytes_signed()?;

            let (remaining, _parsed_transaction) = Transaction::parse(transaction_bytes.as_slice())
                .expect("Parsing of evm transaction failed");

            // Assert
            assert!(signature.is_none(), "An evm transaction doesn't have a bls signature");
            assert!(remaining.is_empty(), "Parsing evm call transaction consumed too few bytes");
        }

        #[test]
        fn ticket_transaction_signature(
            operations in collection::vec(Operation::arb_with_signer(), 1..5),
        ) {
            // Arrange
            let transaction = SendableTransaction::new_ticket_transaction(operations).unwrap();

            let (signature, transaction_bytes) = transaction.to_bytes_signed()?;

            let (remaining, _parsed_transaction) = Transaction::parse(transaction_bytes.as_slice())
                .expect("Parsing of signed transaction failed");

            // Assert
            assert!(signature.is_some(), "A ticket transaction must have signature");
            assert!(remaining.is_empty(), "Parsing evm call transaction consumed too few bytes");
        }

        #[test]
        fn sendable_ticket_transaction_encode_decode(
            operations in collection::vec(Operation::arb_with_signer(), 1..5),
            remaining_input in any::<Vec<u8>>(),
        ) {
            // Arrange
            let transaction = SendableTransaction::new_ticket_transaction(operations).unwrap();
            let mut encoded = Vec::new();
            transaction.bin_write(&mut encoded).unwrap();

            encoded.extend_from_slice(remaining_input.as_slice());

            let (remaining, parsed_transaction) = Transaction::parse(encoded.as_slice())
                .expect("Parsing of encoded ticket transaction failed");

            // Assert
            if let Transaction::TicketTransaction(_t) = parsed_transaction {
                assert!(true);
            } else {
                assert!(false, "Transaction has wrong type - expected ticket transaction");
            }

            assert_eq!(remaining_input, remaining, "Parsing ticket transaction consumed too many bytes");
        }

        #[test]
        fn sendable_batch_encode_decode(
            transactions in collection::vec(
                collection::vec(Operation::arb_with_signer(), 1..5),
                1..5
            ),
            remaining_input in any::<Vec<u8>>(),
        ) {
            // Arrange
            let batch = SendableBatch::new(
                transactions.into_iter()
                .map(|ops| SendableTransaction::new_ticket_transaction(ops).unwrap())
                .collect()
            );

            let mut b = Vec::new();
            let sig = batch.to_bytes_signed(&mut b).unwrap();

            let mut encoded = Vec::new();
            batch.bin_write(&mut encoded).unwrap();

            encoded.extend_from_slice(remaining_input.as_slice());

            let (remaining, parsed_batch) = TransactionBatch::parse(encoded.as_slice())
                .expect("Parsing of encoded batch failed");

            assert_eq!(remaining_input, remaining, "Parsing batch consumed too many bytes");
            assert_eq!(sig, parsed_batch.aggregated_signature, "Expected signatures to be equal");
        }

        #[test]
        fn sendable_batch_with_evm_call(
            common in EthereumTransaction::arb(),
            remaining_input in any::<Vec<u8>>(),
        ) {
            // Arrange
            let transaction = SendableTransaction::new_evm_call(common.clone()).unwrap();
            let batch = SendableBatch::new(vec![transaction]);

            let mut b = Vec::new();
            let sig = batch.to_bytes_signed(&mut b).unwrap();

            let mut encoded = Vec::new();
            batch.bin_write(&mut encoded).unwrap();

            encoded.extend_from_slice(remaining_input.as_slice());

            let (remaining, parsed_batch) = TransactionBatch::parse(encoded.as_slice())
                .expect("Parsing of encoded batch of evm call failed");

            assert_eq!(remaining_input, remaining, "Parsing batch consumed wrong number of bytes");
            assert_eq!(sig, parsed_batch.aggregated_signature, "Expected signatures to be equal");
        }
    }
}
