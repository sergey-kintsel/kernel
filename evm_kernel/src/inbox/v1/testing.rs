// SPDX-FileCopyrightText: 2022-2023 TriliTech <contact@trili.tech>
//
// SPDX-License-Identifier: MIT

//! Generating arbitrary data for testing

use crate::ethereum::address::*;
use crate::ethereum::basic::*;
use crate::ethereum::signatures::EthereumTransaction;
use proptest::prelude::*;

impl EthereumTransaction {
    /// Generate arbitrary Ethereum transaction common data for use with testing
    pub fn arb() -> BoxedStrategy<EthereumTransaction> {
        (
            any::<u8>(),
            proptest::string::string_regex("[0-9]{64}").unwrap(),
            U256::arb(),
            GasPrice::arb(),
            GasPrice::arb(),
            GasLimit::arb(),
            EthereumAddress::arb(),
            Wei::arb(),
        )
            .prop_map(|(t, sk, nonce, g1, g2, gas_limit, to, value)| {
                if t % 2 == 0 {
                    EthereumTransaction::make_classical(
                        sk,
                        1,
                        nonce,
                        g1,
                        gas_limit,
                        vec![],
                        to,
                        value,
                    )
                } else {
                    EthereumTransaction::make_eip1559(
                        sk,
                        1,
                        nonce,
                        g1,
                        g2,
                        gas_limit,
                        vec![],
                        to,
                        value,
                    )
                }
            })
            .boxed()
    }
}
