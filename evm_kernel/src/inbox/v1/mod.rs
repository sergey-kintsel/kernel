// SPDX-FileCopyrightText: 2022-2023 TriliTech <contact@trili.tech>
//
// SPDX-License-Identifier: MIT

//! All version 1 for both transaction kernel and ditto EVM kernel messages

use crate::ethereum::signatures::EthereumTransaction;
use host::rollup_core::RawRollupCore;
use kernel_core::bls::CompressedSignature;
use kernel_core::inbox::external::v1::verifiable::VerifiableTransaction as VerifiableTicketTransaction;
use kernel_core::inbox::external::v1::verifiable::VerificationError;
use kernel_core::storage::AccountStorage as TxAccountStorage;
use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::combinator::map;
use nom::multi::many1;
use nom::sequence::{pair, preceded};
use tezos_encoding::encoding::HasEncoding;
use tezos_encoding::nom::{dynamic, NomReader};

pub mod sendable;

#[cfg(feature = "testing")]
pub mod testing;

/// A de-serialized batch of transactions
///
/// Transactions may be transaction kernel
/// transactions, ie, transfers and withdrawals of tickets. They can also
/// be EVM transactions.
pub struct TransactionBatch<'a> {
    /// The ordered transactions included in the batch.
    pub transactions: Vec<Transaction<'a>>,
    /// The signature for all tx kernel transactions - not the EVM kernel ones.
    /// Iff a batch has only Ethereum transactions, then this will be `None` otherwise
    /// it will be `Some(signature)`.
    pub aggregated_signature: Option<CompressedSignature>,
}

impl<'a> TransactionBatch<'a> {
    /// Parse a batch, where each transaction is either *verifiable* wrt
    /// the transaction kernel, or an Ethereum transaction, which is
    /// verifiable in its own right (through its own signature verification
    /// and EVM interpretation).
    pub fn parse(input: &'a [u8]) -> tezos_encoding::nom::NomResult<Self> {
        map(
            pair(
                dynamic(many1(Transaction::parse)),
                alt((
                    map(tag([0_u8]), |_: &[u8]| {
                        Option::<CompressedSignature>::default()
                    }),
                    preceded(tag([1_u8]), map(CompressedSignature::nom_read, Some)),
                )),
            ),
            |(transactions, aggregated_signature)| TransactionBatch {
                transactions,
                aggregated_signature,
            },
        )(input)
    }

    /// Verify that the batch signature is valid.
    /// TODO: <https://gitlab.com/tezos/tezos/-/issues/3711>
    pub fn verify_signature<Host: RawRollupCore>(
        &self,
        _host: &mut Host,
        _account_storage: &mut TxAccountStorage,
    ) -> Result<(), VerificationError> {
        Ok(())
    }
}

/// Transaction designator
///
/// Decide whether the following transaction is a ticket/TX transaction or
/// an Ethereum transaction.
#[derive(HasEncoding, NomReader)]
enum TransactionType {
    Ticket,
    Ethereum,
}

impl EthereumTransaction {
    /// Verify that signature of transaction is correct and that nonce matched
    /// transaction counter. Funds/assets are checked when transaction is
    /// executed.
    /// TODO: <https://gitlab.com/tezos/tezos/-/milestones/115>
    pub fn verify<Host: RawRollupCore>(
        &self, // TODO: add account information to args to verify caller has funds
        _host: &Host,
    ) -> Result<(), VerificationError> {
        if self.clone().verify_signature() {
            Ok(())
        } else {
            Err(VerificationError::SignatureVerificationError)
        }
    }
}
/// A single transaction for the kernel
///
/// This includes both the transactions inherited from the transactions
/// kernel, _and_ Ethereum transactions.
/// TODO: Reconsider construction of this enum so that it is better
/// balanced. Issue to come in <https://gitlab.com/tezos/tezos/-/milestones/115>.
#[derive(Debug, PartialEq, Eq)]
#[allow(clippy::large_enum_variant)]
pub enum Transaction<'a> {
    /// All transactions for the Ethereum part of the kernel.
    EthereumTransaction(EthereumTransaction),
    /// All transactions involving tickets, ie, ticket -transfers and
    /// -withdrawals.
    TicketTransaction(VerifiableTicketTransaction<'a>),
}

impl<'a> Transaction<'a> {
    /// Parse a single transaction, which is either a standard TX kernel
    /// transaction or an Ethereum transaction.
    pub fn parse(input: &'a [u8]) -> tezos_encoding::nom::NomResult<Self> {
        let (remaining, repr): (&'a [u8], _) = TransactionType::nom_read(input)?;

        match repr {
            TransactionType::Ticket => map(
                VerifiableTicketTransaction::parse,
                Self::TicketTransaction,
            )(remaining),
            TransactionType::Ethereum => {
                map(EthereumTransaction::nom_read, Self::EthereumTransaction)(remaining)
            }
        }
    }

    /// Verify that signature is correct for transaction, either bls signature
    /// for ticket transactions, or ECDSA for Ethereum.
    /// TODO: <https://gitlab.com/tezos/tezos/-/issues/3698>
    pub fn verify_signature<Host: RawRollupCore>(
        self,
        host: &mut Host,
        _account_storage: &mut TxAccountStorage,
    ) -> Result<(), VerificationError> {
        match self {
            Transaction::EthereumTransaction(ethereum_transaction) => {
                ethereum_transaction
                    .verify(host)
                    .map_err(VerificationError::from)
            }
            Transaction::TicketTransaction(_ticket_transaction) => {
                // Nothing to verify - counter and asserts are checked when transaction is
                // executed. Signature is checked on a batch level.
                Ok(())
            }
        }
    }
}
