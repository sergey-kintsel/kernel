// SPDX-FileCopyrightText: 2022-2023 TriliTech <contact@trili.tech>
//
// SPDX-License-Identifier: MIT

//! Types and functions for Ethereum compatibility
//!
//! We need to read and write Ethereum specific values such
//! as addresses and values.
use alloc::rc::Rc;
use evm::executor::stack::PrecompileFailure;
use evm::{Capture, Config, Context, ExitError, ExitFatal, ExitReason, Handler, Resolve};
use host::rollup_core::RawRollupCore;
use primitive_types::{H160, U256};
use thiserror::Error;

pub mod account_storage;
pub mod address;
pub mod basic;
pub mod block;
pub mod handler;
pub mod precompiles;
pub mod rlp;
pub mod signatures;
pub mod storage;

#[cfg(feature = "testing")]
pub mod testing;
pub mod transaction;

use precompiles::PrecompileSet;

/// Errors when processing Ethereum transactions
///
/// What could possibly go wrong? Some of these are place holders for now.
/// When we call an address without code, this should be treated as a simple
/// transfer for instance.
#[derive(Error, Debug, Eq, PartialEq)]
pub enum EthereumError {
    /// A contract was called and the contract doesn't exist.
    #[error("No contract at address: {0}")]
    NoContractAtAddress(H160),
    /// EVM returned with a machine error
    #[error("Internal machine error when running contract")]
    MachineExitError(ExitError),
    /// A fatal error from executing EVM.
    #[error("Fatal machine error when running contract")]
    FatalMachineError(ExitFatal),
    /// Calling a precompiled failed (implies there was a precompiled contract
    /// at the call address.
    #[error("Precompile call failed")]
    PrecompileFailed(PrecompileFailure),
    /// Contract did revert
    #[error("Contract call reverted")]
    CallRevert,
}

/// Call the EVM
pub fn call_evm<'a, Host>(
    host: &'a mut Host,
    block: &'a block::BlockConstants,
    precompiles: &'a precompiles::PrecompileBTreeMap<Host>,
    address: H160,
    caller: H160,
    call_data: &[u8],
) -> Result<(), EthereumError>
where
    Host: RawRollupCore,
{
    let context = Context {
        address,
        caller,
        apparent_value: U256::zero(),
    };
    let dummy_gas_limit = None;
    let dummy_is_static = false;
    let mut handler = handler::EvmHandler::<'a, Host>::new(host, block, context.clone());

    if handler.exists(address) {
        let code = Rc::new(handler.code(address));
        let data: Rc<Vec<u8>> = Rc::new(Vec::from(call_data));
        let config = Config::frontier();
        let mut runtime = evm::Runtime::new(code, data, context, &config);

        let result = match runtime.run(&mut handler) {
            Capture::Exit(ExitReason::Succeed(_exit_succeed)) => {
                Ok(()) // TODO return the output data
            }
            Capture::Exit(ExitReason::Error(error)) => {
                Err(EthereumError::MachineExitError(error))
            }
            Capture::Exit(ExitReason::Revert(_exit_revert)) => {
                Err(EthereumError::CallRevert)
            }
            Capture::Exit(ExitReason::Fatal(fatal_error)) => {
                Err(EthereumError::FatalMachineError(fatal_error))
            }
            Capture::Trap(Resolve::Create(_handler, _config)) => {
                todo!("Create contract")
            }
            Capture::Trap(Resolve::Call(_handler, _config)) => {
                todo!("contract call contract")
            }
        };

        result
    } else if let Some(precompile_result) = precompiles.execute(
        &mut handler,
        address,
        call_data,
        dummy_gas_limit,
        &context,
        dummy_is_static,
    ) {
        match precompile_result {
            Ok(_) => Ok(()),
            Err(e) => Err(EthereumError::PrecompileFailed(e)),
        }
    } else {
        Err(EthereumError::NoContractAtAddress(address))
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use evm::Opcode;
    use mock_runtime::host::MockHost;
    use primitive_types::H160;

    #[test]
    fn call_non_existing_contract() {
        // Arange
        let mut mock_runtime = MockHost::default();
        let block = block::BlockConstants::first_block();
        let precompiles = precompiles::precompile_set::<MockHost>();
        let target = H160::from_low_u64_be(117u64);
        let caller = H160::from_low_u64_be(118u64);
        let data = [0u8; 0];

        // Act
        let result = call_evm(
            &mut mock_runtime,
            &block,
            &precompiles,
            target,
            caller,
            &data,
        );

        // Assert
        let expected_result = Err(EthereumError::NoContractAtAddress(target));
        assert_eq!(result, expected_result);
    }
    #[test]
    //this is based on https://eips.ethereum.org/EIPS/eip-155
    fn test_signatures() {
        let (sk, _address) = signatures::string_to_sk_and_address(
            "4646464646464646464646464646464646464646464646464646464646464646"
                .to_string(),
        );
        let m: [u8; 32] = hex::decode(
            "daf5a779ae972f972197303d7b574746c7ef83eadac0f2791ad23db92e4c8e53"
                .to_string(),
        )
        .unwrap()
        .try_into()
        .unwrap();
        let mes = libsecp256k1::Message::parse(&m);
        let (s, _ri) = libsecp256k1::sign(&mes, &sk);
        assert_eq!(
            hex::encode(s.r.b32()),
            "28ef61340bd939bc2195fe537567866003e1a15d3c71ff63e1590620aa636276"
                .to_string()
        );
        assert_eq!(
            hex::encode(s.s.b32()),
            "67cbe9d8997f761aecb703304b3800ccf555c9f3dc64214b297fb1966a3b6d83"
        )
    }

    #[test]
    fn test_signature_to_address() {
        let test_list = vec![
            (
                "4f3edf983ac636a65a842ce7c78d9aa706d3b113bce9c46f30d7d21715b23b1d",
                "90F8bf6A479f320ead074411a4B0e7944Ea8c9C1",
            ),
            (
                "DC38EE117CAE37750EB1ECC5CFD3DE8E85963B481B93E732C5D0CB66EE6B0C9D",
                "c5ed5d9b9c957be2baa01c16310aa4d1f8bc8e6f",
            ),
            (
                "80b28170e7c2cb2145c052d622ced9de477abcb287e0d23f07263cc30a260534",
                "D0a2dBb5e6F757fd2066a7664f413CAAC504BC95",
            ),
        ];
        test_list.iter().fold((), |_, (s, ea)| {
            let (_, a) = signatures::string_to_sk_and_address(s.to_string());
            let value: [u8; 20] = hex::decode(ea).unwrap().try_into().unwrap();
            let ea = address::EthereumAddress::from(value);
            assert_eq!(a, ea);
        })
    }

    #[test]
    fn test_caller_classic() {
        let (_sk, address_from_sk) = signatures::string_to_sk_and_address(
            "4646464646464646464646464646464646464646464646464646464646464646"
                .to_string(),
        );
        let encoded =
        "f86c098504a817c800825208943535353535353535353535353535353535353535880de0b6b3a76400008025a028ef61340bd939bc2195fe537567866003e1a15d3c71ff63e1590620aa636276a067cbe9d8997f761aecb703304b3800ccf555c9f3dc64214b297fb1966a3b6d83".to_string();
        let transaction = rlp::rlp_decode_classic(encoded);
        let address = transaction.caller();
        let expected_address_string: [u8; 20] =
            hex::decode("9d8A62f656a8d1615C1294fd71e9CFb3E4855A4F".to_string())
                .unwrap()
                .try_into()
                .unwrap();
        let expected_address = address::EthereumAddress::from(expected_address_string);
        assert_eq!(expected_address, address);
        assert_eq!(expected_address, address_from_sk)
    }

    #[test]
    fn test_caller_eip1559() {
        let (_sk, address_from_sk) = signatures::string_to_sk_and_address(
            "528e3019e0a2c484758852ca0ded3c31e96171c9da4112c178c49d1bd43ee896"
                .to_string(),
        );
        let encoded =
        "02f87205018459682f008459682f3682520894d0a2dbb5e6f757fd2066a7664f413caac504bc9588016345785d8a000080c001a02bade97eabf34bf84b5e02e342a69eb2ed755fc7c7a7fdbec1bb878b5da2a074a0600035bcf379bc8f0d30d7f85772b81a56acdc5594ce6cdca5169eca9570d169".to_string();
        let tr = rlp::rlp_decode_eip_1559(encoded);
        let add = tr.caller();

        let exp: [u8; 20] =
            hex::decode("8E998A00253Cb1747679AC25e69A8d870B52d889".to_string())
                .unwrap()
                .try_into()
                .unwrap();

        let expaddress = address::EthereumAddress::from(exp);
        assert_eq!(expaddress, add);
        assert_eq!(expaddress, address_from_sk)
    }

    #[test]
    fn test_signed_classic_transaction() {
        let string_sk =
            "4646464646464646464646464646464646464646464646464646464646464646"
                .to_string();
        let encoded =
        "f86c098504a817c800825208943535353535353535353535353535353535353535880de0b6b3a76400008025a028ef61340bd939bc2195fe537567866003e1a15d3c71ff63e1590620aa636276a067cbe9d8997f761aecb703304b3800ccf555c9f3dc64214b297fb1966a3b6d83".to_string();
        let expected_transaction = rlp::rlp_decode_classic(encoded);

        let transaction = expected_transaction.sign_transaction(string_sk);
        assert_eq!(expected_transaction, transaction)
    }
    #[test]
    fn test_signed_eip1559_transaction() {
        let string_sk =
            "528e3019e0a2c484758852ca0ded3c31e96171c9da4112c178c49d1bd43ee896"
                .to_string();
        let encoded =
        "02f87205018459682f008459682f3682520894d0a2dbb5e6f757fd2066a7664f413caac504bc9588016345785d8a000080c001a02bade97eabf34bf84b5e02e342a69eb2ed755fc7c7a7fdbec1bb878b5da2a074a0600035bcf379bc8f0d30d7f85772b81a56acdc5594ce6cdca5169eca9570d169".to_string();
        let expected_transaction = rlp::rlp_decode_eip_1559(encoded);
        let transaction = expected_transaction.sign_eip1559_transaction(string_sk);
        assert_eq!(expected_transaction, transaction)
    }

    #[test]
    fn call_simple_return_contract() {
        // Arrange
        let mut mock_runtime = MockHost::default();
        let block = block::BlockConstants::first_block();
        let precompiles = precompiles::precompile_set::<MockHost>();
        let target = H160::from_low_u64_be(117u64);
        let caller = H160::from_low_u64_be(118u64);
        let data = [0u8; 0];
        let code = vec![
            Opcode::PUSH1.as_u8(),
            0u8,
            Opcode::PUSH1.as_u8(),
            0u8,
            Opcode::RETURN.as_u8(),
        ];

        storage::set_code(&mut mock_runtime, target, &code);

        // Act
        let result = call_evm(
            &mut mock_runtime,
            &block,
            &precompiles,
            target,
            caller,
            &data,
        );

        // Assert
        let expected_result = Ok(());
        assert_eq!(result, expected_result);
    }

    #[test]
    fn call_simple_revert_contract() {
        // Arrange
        let mut mock_runtime = MockHost::default();
        let block = block::BlockConstants::first_block();
        let precompiles = precompiles::precompile_set::<MockHost>();
        let target = H160::from_low_u64_be(117u64);
        let caller = H160::from_low_u64_be(118u64);
        let data = [0u8; 0];
        let code = vec![
            Opcode::PUSH1.as_u8(),
            0u8,
            Opcode::PUSH1.as_u8(),
            0u8,
            Opcode::REVERT.as_u8(),
        ];

        storage::set_code(&mut mock_runtime, target, &code);

        // Act
        let result = call_evm(
            &mut mock_runtime,
            &block,
            &precompiles,
            target,
            caller,
            &data,
        );

        // Assert
        let expected_result = Err(EthereumError::CallRevert);
        assert_eq!(result, expected_result);
    }

    #[test]
    fn call_contract_with_invalid_opcode() {
        // Arrange
        let mut mock_runtime = MockHost::default();
        let block = block::BlockConstants::first_block();
        let precompiles = precompiles::precompile_set::<MockHost>();
        let target = H160::from_low_u64_be(117u64);
        let caller = H160::from_low_u64_be(118u64);
        let data = [0u8; 0];
        let code = vec![
            Opcode::INVALID.as_u8(),
            Opcode::PUSH1.as_u8(),
            0u8,
            Opcode::PUSH1.as_u8(),
            0u8,
            Opcode::REVERT.as_u8(),
        ];

        storage::set_code(&mut mock_runtime, target, &code);

        // Act
        let result = call_evm(
            &mut mock_runtime,
            &block,
            &precompiles,
            target,
            caller,
            &data,
        );

        // Assert
        let expected_result = Err(EthereumError::MachineExitError(
            ExitError::DesignatedInvalid,
        ));
        assert_eq!(result, expected_result);
    }

    #[test]
    fn call_precompiled_contract() {
        // Arrange
        let mut mock_runtime = MockHost::default();
        let block = block::BlockConstants::first_block();
        let precompiles = precompiles::precompile_set::<MockHost>();
        let target = H160::from_low_u64_be(4u64);
        let caller = H160::from_low_u64_be(118u64);
        let data = [0u8; 0];

        // Act
        let result = call_evm(
            &mut mock_runtime,
            &block,
            &precompiles,
            target,
            caller,
            &data,
        );

        // Assert
        let expected_result = Ok(());

        assert_eq!(result, expected_result);
    }
}
