// SPDX-FileCopyrightText: 2022-2023 TriliTech <contact@trili.tech>
// SPDX-FileCopyrightText: 2023 Marigold <contact@marigold.dev>
//
// SPDX-License-Identifier: MIT

//! Signature functions for Ethereum compatibility
//!
//! We need to sign and write Ethereum specific values such
//! as addresses and values.

use crate::ethereum::address::EthereumAddress;
use crate::ethereum::basic::{GasLimit, GasPrice, Wei, H256, U256};
use crate::ethereum::rlp::*;
use libsecp256k1::{
    curve::Scalar, recover, sign, verify, Message, PublicKey, RecoveryId, SecretKey,
    Signature,
};
use primitive_types::H256 as PTH256;
use sha3::{Digest, Keccak256};
use tezos_encoding::enc::BinWriter;
use tezos_encoding::encoding::HasEncoding;
use tezos_encoding::nom::NomReader;

/// produces address from a secret key
pub fn string_to_sk_and_address(s: String) -> (SecretKey, EthereumAddress) {
    let data: [u8; 32] = hex::decode(s).unwrap().try_into().unwrap();
    let sk = SecretKey::parse(&data).unwrap();
    let pk = PublicKey::from_secret_key(&sk);
    let serialised = &pk.serialize()[1..];
    let kec = Keccak256::digest(serialised);
    let value: [u8; 20] = kec.as_slice()[12..].try_into().unwrap();
    (sk, EthereumAddress::from(value))
}

///umbrella tye fro transactions
#[derive(Debug, PartialEq, Eq, Clone, HasEncoding, NomReader, BinWriter)]
pub enum EthereumTransaction {
    /// classical
    Classical(EthereumTransactionCommon),
    /// EIP=1559
    EIP1559(EthereumEIP1559Transaction),
}

/// the type of a transaction
#[derive(Debug, PartialEq, Eq, Clone, HasEncoding, NomReader, BinWriter)]

pub enum EthereumTransactionType {
    /// transfer
    EthereumTransfer,
    ///create
    EthereumCreate,
    /// call
    EthereumCall,
}

impl EthereumTransaction {
    ///
    pub fn caller(&self) -> EthereumAddress {
        match self {
            Self::Classical(e) => e.caller(),
            Self::EIP1559(e) => e.caller(),
        }
    }
    ///
    pub fn verify_signature(self) -> bool {
        match self {
            Self::Classical(e) => e.verify_signature(),
            Self::EIP1559(e) => e.verify_signature(),
        }
    }
    ///create classical transaction
    #[allow(clippy::too_many_arguments)]

    pub fn make_classical(
        string_sk: String,
        chain_id: u8,
        nonce: U256,
        gas_price: GasPrice,
        gas_limit: GasLimit,
        data: Vec<u8>,
        to: EthereumAddress,
        value: Wei,
    ) -> Self {
        let s = EthereumTransactionCommon {
            chain_id,
            data,
            nonce,
            gas_price,
            gas_limit,
            to,
            value,
            r: H256::zero(),
            s: H256::zero(),
            v: 1u8,
        }
        .sign_transaction(string_sk);
        Self::Classical(s)
    }
    ///create classical transaction
    #[allow(clippy::too_many_arguments)]
    pub fn make_eip1559(
        string_sk: String,
        chain_id: u8,
        nonce: U256,
        max_priority_fee_per_gas: GasPrice,
        max_fee_per_gas: GasPrice,
        gas_limit: GasLimit,
        data: Vec<u8>,
        to: EthereumAddress,
        value: Wei,
    ) -> Self {
        let s = EthereumEIP1559Transaction {
            chain_id,
            data,
            nonce,
            max_priority_fee_per_gas,
            max_fee_per_gas,
            gas_limit,
            to,
            value,
            r: H256::zero(),
            s: H256::zero(),
            v: 1u8,
        }
        .sign_eip1559_transaction(string_sk);
        Self::EIP1559(s)
    }
    /// computes the type of a transaction
    pub fn transaction_type(&self) -> EthereumTransactionType {
        match self {
            Self::Classical(e) => {
                if e.to == EthereumAddress::from_u64_be(0) {
                    EthereumTransactionType::EthereumCreate
                } else if e.data.is_empty() {
                    EthereumTransactionType::EthereumTransfer
                } else {
                    EthereumTransactionType::EthereumCall
                }
            }
            Self::EIP1559(e) => {
                if e.to == EthereumAddress::from_u64_be(0) {
                    EthereumTransactionType::EthereumCreate
                } else if e.data.is_empty() {
                    EthereumTransactionType::EthereumTransfer
                } else {
                    EthereumTransactionType::EthereumCall
                }
            }
        }
    }
}
impl From<String> for EthereumTransaction {
    fn from(e: String) -> Self {
        if e.get(0..2) == Some("02") {
            let t: EthereumEIP1559Transaction = e.into();
            Self::EIP1559(t)
        } else {
            let t: EthereumTransactionCommon = e.into();
            Self::Classical(t)
        }
    }
}
#[test]
fn test_create() {
    let data = [
        ("02f901f20580849502f900850dba2e41aa83020d138080b90198608060405234801561001057600080fd5b50610178806100206000396000f3fe608060405234801561001057600080fd5b506004361061002b5760003560e01c806354f8a2f214610030575b600080fd5b61003861004e565b60405161004591906100fe565b60405180910390f35b606060405180606001604052806022815260200161012160229139905090565b600081519050919050565b600082825260208201905092915050565b60005b838110156100a857808201518184015260208101905061008d565b60008484015250505050565b6000601f19601f8301169050919050565b60006100d08261006e565b6100da8185610079565b93506100ea81856020860161008a565b6100f3816100b4565b840191505092915050565b6000602082019050818103600083015261011881846100c5565b90509291505056fe48692c20796f757220636f6e74726163742072616e207375636365737366756c6c79a26469706673582212201e6723082f7bbf9b947480367c28d05f22751d48c4c0b66d548a31c5fc7c599f64736f6c63430008120033c080a0efadbb644c401af7d975fc157db259800e76dc5ee70adc6f1fcbdbf58681cc77a0496e82af73217577941d98f971b319cef636462362011ae378e5515c3770a01c".to_string(), EthereumTransactionType::EthereumCreate),
    ("f86c0a8502540be400825208944bbeeb066ed09b7aed07bf39eee0460dfa261520880de0b6b3a7640000801ca0f3ae52c1ef3300f44df0bcfd1341c232ed6134672b16e35699ae3f5fe2493379a023d23d2955a239dd6f61c4e8b2678d174356ff424eac53da53e17706c43ef871".to_string(), EthereumTransactionType::EthereumTransfer),
    ("02f901b20148843b9aca0085089792c0fe8302ab7194e1b81cd6a494cbca06a8e2055a62c2cf0fa5a8ac80b901441774821500000000000000000000000000000000000000000000000000000000000011d0000000000000000000000000000000000000000000000000000000000000008000000000000000000000000000000000000000000000000000000000000000a00000000000000000000000000000000000000000000000000000000000000120000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000030000000000000000000000000000000000000000000000000000000000000d59000000000000000000000000000000000000000000000000000000000000106a00000000000000000000000000000000000000000000000000000000000014dc0000000000000000000000000000000000000000000000000000000000000000c080a07858dfdccd26f9287adb9950307bca3522297cb7d40b46966ae4dc4cc1d30e7da040ef3c0ce19eb9e903e01a052889759a70305732cb618b9168166dbfc68cb931".to_string(), EthereumTransactionType::EthereumCall)];
    data.iter().fold((), |_, (string_trans, trans_type)| {
        let trans: EthereumTransaction = string_trans.clone().into();
        assert_eq!(trans.transaction_type(), trans_type.clone())
    })
}

/// Data common to all Ethereum transaction types
#[derive(Debug, PartialEq, Eq, Clone, HasEncoding, NomReader, BinWriter)]

pub struct EthereumTransactionCommon {
    /// the id of the chain
    /// see `<https://chainlist.org/>` for values
    pub chain_id: u8,
    /// A scalar value equal to the number of trans- actions sent by the sender
    pub nonce: U256,
    /// A scalar value equal to the number of
    /// Wei to be paid per unit of gas for all computation
    /// costs incurred as a result of the execution of this
    /// transaction
    pub gas_price: GasPrice,
    /// A scalar value equal to the maximum
    /// amount of gas that should be used in executing
    /// this transaction. This is paid up-front, before any
    /// computation is done and may not be increased
    /// later
    pub gas_limit: GasLimit,
    /// The 160-bit address of the message call’s recipi-
    /// ent or, for a contract creation transaction
    ///
    pub to: EthereumAddress,
    /// A scalar value equal to the number of Wei to
    /// be transferred to the message call’s recipient or,
    /// in the case of contract creation, as an endowment
    /// to the newly created account
    pub value: Wei,
    /// the transaction data. In principle this can be large
    ///
    #[encoding(dynamic, list)]
    pub data: Vec<u8>,
    /// Signature x-axis part of point on elliptic curve. See yellow paper, appendix F
    pub r: H256,
    /// Signature, See yellow paper appendix F
    pub s: H256,
    /// the parity (recovery id) of the signature See yellow paper appendix F
    pub v: u8, //
}

impl EthereumTransactionCommon {
    /// Extracts the Keccak encoding of a message from an EthereumTransactionCommon
    pub fn message(&self) -> Message {
        let m = rlp_encode_for_sign(self.clone());
        let mm: &[u8] = &hex::decode(m.as_bytes()).unwrap();
        let m: [u8; 32] = Keccak256::digest(mm).into();
        let tt = PTH256::from(m);
        let mes = Message::parse(tt.as_fixed_bytes());
        mes
    }

    /// Extracts the signature from an EthereumTransactionCommon
    pub fn signature(&self) -> (Signature, RecoveryId) {
        let EthereumTransactionCommon { r, s, v, .. } = self.clone();
        let r: PTH256 = H256::into(r);
        let r1: [u8; 32] = r.into();
        let mut r = Scalar([0; 8]);
        let _ = r.set_b32(&r1);
        let s: PTH256 = H256::into(s);
        let s1: [u8; 32] = s.into();
        let mut s = Scalar([0; 8]);
        let _ = s.set_b32(&s1);
        let ri = RecoveryId::parse((v + 1) % 2).unwrap();

        (Signature { r, s }, ri)
    }
    /// Find the caller address from r and s of the common data
    /// for an Ethereum transaction, ie, what address is associated
    /// with the signature of the message.
    /// TODO <https://gitlab.com/tezos/tezos/-/milestones/115>
    pub fn caller(&self) -> EthereumAddress {
        let mes = self.message();
        let (sig, ri) = self.signature();
        let pk = recover(&mes, &sig, &ri).unwrap();
        let serialised = &pk.serialize()[1..];
        let kec = Keccak256::digest(serialised);
        let value: [u8; 20] = kec.as_slice()[12..].try_into().unwrap();
        EthereumAddress::from(value)
    }

    ///produce a signed EthereumTransactionCommon. If the initial one was signed
    ///  you should get the same thing.

    pub fn sign_transaction(&self, string_sk: String) -> Self {
        let (sk, _) = string_to_sk_and_address(string_sk);
        let mes = rlp_encode_for_sign(self.clone());
        let hex_mes: &[u8] = &hex::decode(mes.as_bytes()).unwrap()[..];
        let t: [u8; 32] = Keccak256::digest(hex_mes).into();
        let mes = Message::parse(&t);
        let (sig, ri) = sign(&mes, &sk);
        let Signature { r, s } = sig;
        let (r, s) = (
            H256::from(PTH256::from(r.b32())),
            H256::from(PTH256::from(s.b32())),
        );

        let parity: u8 = ri.into();
        let v = if self.chain_id == 0 {
            27 + parity
        } else {
            parity + 2 * self.chain_id + 35
        };
        EthereumTransactionCommon {
            v,
            r,
            s,
            ..self.clone()
        }
    }

    /// verifies the signature
    pub fn verify_signature(self) -> bool {
        let mes = self.message();
        let (sig, ri) = self.signature();
        let pk = recover(&mes, &sig, &ri).unwrap();
        verify(&mes, &sig, &pk)
    }
}

impl From<String> for EthereumTransactionCommon {
    fn from(e: String) -> Self {
        rlp_decode_classic(e)
    }
}

/// Data common to all Ethereum transaction types
#[derive(Debug, PartialEq, Eq, Clone, HasEncoding, NomReader, BinWriter)]

pub struct EthereumEIP1559Transaction {
    /// the id of the chain
    /// see `<https://chainlist.org/>` for values
    pub chain_id: u8,
    /// A scalar value equal to the number of trans- actions sent by the sender
    pub nonce: U256,
    /// EIP-1559 transaction type
    /// A scalar value equal to the maximum number of
    /// Wei  of gas to be included as a tip to the miner    
    pub max_priority_fee_per_gas: GasPrice,
    /// the maximum amount of gas willing to be paid for the transaction
    /// (inclusive of baseFeePerGas and maxPriorityFeePerGas)
    pub max_fee_per_gas: GasPrice,
    /// A scalar value equal to the maximum
    /// amount of gas that should be used in executing
    /// this transaction. This is paid up-front, before any
    /// computation is done and may not be increased
    /// later
    pub gas_limit: GasLimit,
    /// The 160-bit address of the message call’s recipi-
    /// ent or, for a contract creation transaction
    ///
    pub to: EthereumAddress,
    /// A scalar value equal to the number of Wei to
    /// be transferred to the message call’s recipient or,
    /// in the case of contract creation, as an endowment
    /// to the newly created account
    pub value: Wei,
    /// the transaction data. In principle this can be large
    #[encoding(dynamic, list)]
    pub data: Vec<u8>,
    //TODO: needs to have an accessList.
    /// Signature x-axis part of point on elliptic curve. See yellow paper, appendix F
    pub r: H256,
    /// Signature, See yellow paper appendix F
    pub s: H256,
    ///
    pub v: u8, //
}
impl EthereumEIP1559Transaction {
    /// extracts the message to be signed
    pub fn message(&self) -> Message {
        let m = rlp_encode_eip1559_for_sign(self.clone());
        let mm: &[u8] = &hex::decode(m.as_bytes()).unwrap()[..];
        let m: [u8; 32] = Keccak256::digest(mm).into();
        let tt = PTH256::from(m);
        let mes = Message::parse(tt.as_fixed_bytes());
        mes
    }

    /// Extracts the signature from an EthereumTransactionCommon
    pub fn signature(self) -> (Signature, RecoveryId) {
        let EthereumEIP1559Transaction { r, s, v, .. } = self;
        let r: PTH256 = H256::into(r);
        let r1: [u8; 32] = r.into();
        let mut r = Scalar([0; 8]);
        let _ = r.set_b32(&r1);
        let s: PTH256 = H256::into(s);
        let s1: [u8; 32] = s.into();
        let mut s = Scalar([0; 8]);
        let _ = s.set_b32(&s1);
        let ri = RecoveryId::parse(v).unwrap();
        (Signature { r, s }, ri)
    }
    /// Find the caller address from r and s of the common data
    /// for an Ethereum transaction, ie, what address is associated
    /// with the signature of the message.
    /// TODO <https://gitlab.com/tezos/tezos/-/milestones/115>

    pub fn caller(&self) -> EthereumAddress {
        let mes = self.message();
        let (sig, ri) = self.clone().signature();
        let pk = recover(&mes, &sig, &ri).unwrap();
        let serialised = &pk.serialize()[1..];
        let kec = Keccak256::digest(serialised);
        let value: [u8; 20] = kec.as_slice()[12..].try_into().unwrap();
        EthereumAddress::from(value)
    }
    /// Produce a signed EIP1559 message
    pub fn sign_eip1559_transaction(
        &self,
        string_sk: String,
    ) -> EthereumEIP1559Transaction {
        let (sk, _) = string_to_sk_and_address(string_sk);
        let mes = &rlp_encode_eip1559_for_sign(self.clone());
        let hex_mes: &[u8] = &hex::decode(mes.as_bytes()).unwrap()[..];
        let t: [u8; 32] = Keccak256::digest(hex_mes).into();

        let mes = Message::parse(&t);
        let (sig, ri) = sign(&mes, &sk);

        let Signature { r, s } = sig;
        let (r, s) = (
            H256::from(PTH256::from(r.b32())),
            H256::from(PTH256::from(s.b32())),
        );
        let v = ri.into();

        EthereumEIP1559Transaction {
            r,
            s,
            v,
            ..self.clone()
        }
    }

    /// verifies the signature
    pub fn verify_signature(self) -> bool {
        let mes = self.message();
        let (sig, ri) = self.signature();
        let pk = recover(&mes, &sig, &ri).unwrap();
        verify(&mes, &sig, &pk)
    }
}
impl From<String> for EthereumEIP1559Transaction {
    fn from(e: String) -> Self {
        rlp_decode_eip_1559(e)
    }
}
