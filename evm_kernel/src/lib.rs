// SPDX-FileCopyrightText: 2022-2023 TriliTech <contact@trili.tech>
// SPDX-FileCopyrightText: 2023 Marigold <contact@marigold.dev>
// SPDX-FileCopyrightText: 2022-2023 Nomadic Labs <contact@nomadic-labs.com>
//
// SPDX-License-Identifier: MIT

//! Contains the EVM kernel
//!
//! This kernel runs EVM contract code emulating Ethereum, but on a rollup.
//! Alongside the EVM functionality, the kernel also implements **TORU** style
//! transactions kernel functionality.
#![deny(missing_docs)]
#![deny(rustdoc::all)]
#![forbid(unsafe_code)]

extern crate alloc;

pub mod ethereum;
pub mod inbox;
pub mod transactions;

use debug::debug_msg;
use host::rollup_core::{
    RawRollupCore, MAX_INPUT_MESSAGE_SIZE, MAX_INPUT_SLOT_DATA_CHUNK_SIZE,
};
use host::runtime::{Runtime, RuntimeError};
use kernel_core::encoding::michelson::{MichelsonPair, MichelsonString};
use kernel_core::encoding::string_ticket::StringTicketRepr;
use kernel_core::inbox::DepositFromInternalPayloadError;
use kernel_core::storage::{
    deposit_ticket_to_storage, init_account_storage as init_tx_account_storage,
    AccountStorage as TxAccountStorage, AccountStorageError,
};
use kernel_core::transactions::withdrawal::process_withdrawals;
use tezos_encoding::nom::error::DecodeError;
use thiserror::Error;

use crate::ethereum::basic::{H256, U256};
use crate::ethereum::block::BlockConstants;
use crate::ethereum::storage::blocks::EvmBlockStorageError;
use crate::inbox::{InboxDeposit, InboxMessage, InternalInboxMessage, Transfer};
use crate::transactions::external_inbox;

const MAX_READ_INPUT_SIZE: usize =
    if MAX_INPUT_MESSAGE_SIZE > MAX_INPUT_SLOT_DATA_CHUNK_SIZE {
        MAX_INPUT_MESSAGE_SIZE
    } else {
        MAX_INPUT_SLOT_DATA_CHUNK_SIZE
    };

/// Entrypoint of the *evm* kernel.
pub fn evm_kernel_run<Host: RawRollupCore>(host: &mut Host) {
    let mut tx_account_storage = match init_tx_account_storage() {
        Ok(v) => v,
        Err(err) => {
            panic!("Could not initialize TX kernel account storage: {:?}", err);
        }
    };

    match host.read_input(MAX_READ_INPUT_SIZE) {
        Ok(Some(message)) => {
            debug_msg!(
                Host,
                "Processing MessageData {} at level {}",
                message.id,
                message.level
            );

            if let Err(err) = process_inbox_message(
                host,
                &mut tx_account_storage,
                message.level,
                message.as_ref(),
            ) {
                debug_msg!(Host, "Error processing header payload {:?}", err);
            }
        }
        Ok(None) => {}
        Err(_) => todo!("handle runtime errors"),
    }
}

/// Anything that can go wrong on the application level
#[derive(Error, Debug)]
pub enum ApplicationError<'a> {
    /// Something went wrong while working with Ticket Account storage
    #[error("Error operating durable ticket account storage:  {0}")]
    TicketAccountStorageError(#[from] AccountStorageError),
    /// Error parsing inbox message
    #[error("unable to parse header inbox message {0}")]
    MalformedInboxMessage(nom::Err<DecodeError<&'a [u8]>>),
    /// Error getting a deposit from an internal message
    #[error("Could not get deposit from internal inbox message: {0}")]
    DepositMessageError(#[from] DepositFromInternalPayloadError),
    /// Error happened during interaction with the storage
    #[error("EVM storage failed: {0}")]
    EvmStorage(#[from] EvmBlockStorageError),
    /// Error fetching rollup metadata from host
    #[error("Failed to fetch rollup metadata: {0:?}")]
    FailedToFetchRollupMetadata(RuntimeError),
}

fn process_inbox_message<'a, Host: RawRollupCore>(
    host: &mut Host,
    tx_account_storage: &mut TxAccountStorage,
    level: i32,
    message: &'a [u8],
) -> Result<(), ApplicationError<'a>> {
    let precompiles = ethereum::precompiles::precompile_set();

    let (remaining, message) =
        InboxMessage::<MichelsonPair<MichelsonString, StringTicketRepr>>::parse(message)
            .map_err(ApplicationError::MalformedInboxMessage)?;

    match message {
        InboxMessage::Internal(InternalInboxMessage::Transfer(Transfer {
            payload,
            ..
        })) => {
            let InboxDeposit {
                destination,
                ticket,
            } = payload.try_into()?;

            deposit_ticket_to_storage(host, tx_account_storage, &destination, &ticket)?;

            debug_msg!(Host, "Deposited ticket {:?} to {}", ticket, destination);

            debug_assert!(remaining.is_empty(), "Internal inbox messages are not batched, there should be no remaining bytes");

            Ok(())
        }
        InboxMessage::Internal(InternalInboxMessage::InfoPerLevel(info)) => {
            debug_msg!(host; "InfoPerLevel: {}", info);
            Runtime::reveal_metadata(host)
                .map_err(ApplicationError::FailedToFetchRollupMetadata)
                .and_then(|metadata| {
                    // Origination level considered to be block level with number 0
                    // Next one is with number 1
                    let block_number =
                        U256::from(level - metadata.origination_level).into();
                    ethereum::storage::blocks::add_new_block(
                        host,
                        block_number,
                        H256::from(info.predecessor).into(),
                        U256::from(info.predecessor_timestamp).into(),
                    )
                    .map_err(ApplicationError::EvmStorage)
                })
        }

        InboxMessage::Internal(
            msg @ (InternalInboxMessage::StartOfLevel | InternalInboxMessage::EndOfLevel),
        ) => {
            debug_msg!(host; "InboxMetadata: {}", msg);
            Ok(())
        }
        InboxMessage::External(message) => {
            let current_block = BlockConstants::from_storage::<Host>(host);
            external_inbox::process_external(
                host,
                tx_account_storage,
                &current_block,
                &precompiles,
                message,
            )
            .into_iter()
            .for_each(|withdrawals| process_withdrawals(host, withdrawals));

            Ok(())
        }
    }
}

/// Define the `kernel_run` for the transactions kernel.
#[cfg(feature = "evm_kernel")]
pub mod evm_kernel {
    use crate::evm_kernel_run;
    use kernel::kernel_entry;
    kernel_entry!(evm_kernel_run);
}

#[cfg(test)]
mod test {
    use kernel_core::encoding::{
        contract::Contract,
        michelson::{MichelsonContract, MichelsonPair},
        public_key_hash::PublicKeyHash,
        smart_rollup::SmartRollupAddress,
        string_ticket::StringTicket,
    };
    use tezos_encoding::enc::BinWriter;

    use super::*;

    use crypto::hash::{ContractKt1Hash, ContractTz4Hash, HashTrait};
    use mock_runtime::host::MockHost;

    use host::path::Path;
    use kernel_core::encoding::string_ticket::StringTicketHash;
    use kernel_core::storage::account_path;

    fn ticket_amount<Host: RawRollupCore>(
        host: &Host,
        account_path: &impl Path,
        ticket_id: &StringTicketHash,
    ) -> u64 {
        let tx_account_storage = match init_tx_account_storage() {
            Ok(v) => v,
            Err(err) => panic!("Could not read account storage while testing: {:?}", err),
        };

        match tx_account_storage.get_account(host, account_path) {
            Ok(None) => 0_u64,
            Ok(Some(account)) => match account.ticket_amount(host, ticket_id) {
                Ok(Some(v)) => v,
                Ok(None) => 0_u64,
                Err(err) => {
                    panic!("Error when getting ticket amount while testing: {:?}", err)
                }
            },
            Err(err) => panic!("Error when getting account while testing: {:?}", err),
        }
    }

    #[test]
    fn deposit_ticket() {
        // Arrange
        let mut mock_runtime = MockHost::default();

        let ticket_destination =
            ContractTz4Hash::from_b58check("tz4MSfZsn6kMDczShy8PMeB628TNukn9hi2K")
                .unwrap();

        let ticket_creator =
            Contract::from_b58check("KT1JW6PwhfaEJu6U3ENsxUeja48AdtqSoekd").unwrap();
        let ticket_content = "hello, ticket!";
        let ticket_quantity = 5;

        let message =
            InboxMessage::<MichelsonPair<MichelsonString, StringTicketRepr>>::Internal(
                InternalInboxMessage::Transfer(Transfer {
                    payload: MichelsonPair(
                        MichelsonString(ticket_destination.to_base58_check()),
                        MichelsonPair(
                            MichelsonContract(ticket_creator.clone()),
                            MichelsonPair(
                                MichelsonString(ticket_content.to_string()),
                                ticket_quantity.into(),
                            ),
                        ),
                    ),
                    sender: ContractKt1Hash::from_b58check(
                        "KT1VsSxSXUkgw6zkBGgUuDXXuJs9ToPqkrCg",
                    )
                    .unwrap(),
                    source: PublicKeyHash::from_b58check(
                        "tz3SvEa4tSowHC5iQ8Aw6DVKAAGqBPdyK1MH",
                    )
                    .unwrap(),
                    destination: SmartRollupAddress::new(
                        mock_runtime.as_mut().get_metadata().address().unwrap(),
                    ),
                }),
            );

        let mut input = Vec::new();
        message.bin_write(&mut input).unwrap();

        let level = 10;
        mock_runtime.as_mut().set_ready_for_input(level);
        mock_runtime
            .as_mut()
            .add_next_inputs(10, [input.as_slice()].into_iter());

        // Act
        evm_kernel_run(&mut mock_runtime);

        // Assert
        let expected_ticket = StringTicket::new(
            ticket_creator,
            ticket_content.to_string(),
            ticket_quantity as u64,
        );
        assert_eq!(
            ticket_amount(
                &mock_runtime,
                &account_path(&ticket_destination).unwrap(),
                &expected_ticket.identify().unwrap()
            ),
            5_u64
        );
    }

    #[ignore]
    #[test]
    fn deposit_fails_on_invalid_destination() {
        // Arrange
        let mut mock_runtime = MockHost::default();

        let ticket_destination = "tz4BAD";

        let ticket_creator =
            Contract::from_b58check("KT1JW6PwhfaEJu6U3ENsxUeja48AdtqSoekd").unwrap();
        let ticket_content = "hello, ticket!";
        let ticket_quantity = 5;

        let message =
            InboxMessage::<MichelsonPair<MichelsonString, StringTicketRepr>>::Internal(
                InternalInboxMessage::Transfer(Transfer {
                    payload: MichelsonPair(
                        MichelsonString(ticket_destination.to_string()),
                        MichelsonPair(
                            MichelsonContract(ticket_creator.clone()),
                            MichelsonPair(
                                MichelsonString(ticket_content.to_string()),
                                ticket_quantity.into(),
                            ),
                        ),
                    ),
                    sender: ContractKt1Hash::from_b58check(
                        "KT1VsSxSXUkgw6zkBGgUuDXXuJs9ToPqkrCg",
                    )
                    .unwrap(),
                    source: PublicKeyHash::from_b58check(
                        "tz3SvEa4tSowHC5iQ8Aw6DVKAAGqBPdyK1MH",
                    )
                    .unwrap(),
                    destination: SmartRollupAddress::new(
                        mock_runtime.as_mut().get_metadata().address().unwrap(),
                    ),
                }),
            );

        let mut input = Vec::new();
        message.bin_write(&mut input).unwrap();

        let level = 10;
        mock_runtime.as_mut().set_ready_for_input(level);
        mock_runtime
            .as_mut()
            .add_next_inputs(10, [input.as_slice()].into_iter());

        // Act
        evm_kernel_run(&mut mock_runtime);

        // Assert
        todo!("Verify that ticket does not exist on the rollup at all. This can only be done when we have implemented the rollup balance");
    }
}
