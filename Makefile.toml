# SPDX-FileCopyrightText: 2022 TriliTech <contact@trili.tech>
#
# SPDX-License-Identifier: MIT

[env]
CARGO_MAKE_EXTEND_WORKSPACE_MAKEFILE = true

[tasks.clean]
command = "cargo"
args = ["clean"]

[tasks.fmt]
command = "cargo"
args = ["fmt"]

[tasks.fmt-check]
command = "cargo"
args = ["fmt", "--", "--check"]

[tasks.check-disable-defaults]
workspace = false
command = "cargo"
args = ["check", "--no-default-features"]

[tasks.check]
workspace = false
command = "cargo"
args = ["check", "--features", "testing"]

[tasks.clippy]
workspace = false
command = "cargo"
args = ["clippy", "--features", "testing", "--", "--deny", "warnings"]

[tasks.wasm-tx-kernel]
workspace = false
command = "cargo"
args = ["build", "--release",
        "-p", "kernel-core",
        "--target", "wasm32-unknown-unknown",
        "--features", "tx-kernel"]

[tasks.wasm-tx-kernel-no-sig-verif]
workspace = false
command = "cargo"
args = ["build", "--release",
        "-p", "kernel-core",
        "--target", "wasm32-unknown-unknown",
        "--features", "tx-kernel-no-sig-verif"]

[tasks.wasm-evm-kernel]
command = "cargo"
args = ["build", "--release", "--target", "wasm32-unknown-unknown", "-p", "evm-kernel"]
workspace = false

[tasks.wasm-preimage-installer]
workspace = false
command = "cargo"
args = ["build", "--release",
        "--target", "wasm32-unknown-unknown",
        "-p", "tezos_rollup_installer_kernel",
        "--features", "preimage-installer"]

[tasks.wasm-tx-demo-installer]
workspace = false
command = "cargo"
args = ["build", "--release",
        "--target", "wasm32-unknown-unknown",
        "-p", "tezos_rollup_installer_kernel",
        "--features", "tx-demo-installer"]

[tasks.wasm-test-kernel]
workspace = false
command = "cargo"
args = ["build,", "-p", "test-kernel", "--release", "--target", "wasm32-unknown-unknown", "--no-default-features", "--features=${@}"]

[tasks.test-e2e]
command = "cargo"
args = ["test", "-p", "e2e_kernel_tests", "--features", "testing"]
workspace = false

[tasks.test]
workspace = false
command = "cargo"
args = ["test", "--features", "testing"]

[tasks.doc]
workspace = false
command = "cargo"
args = ["doc", "--features", "testing", "--no-deps", "--target-dir", "public"]

[tasks.ci]
workspace = false
dependencies = [
  "clean",
  "fmt-check",
  "check",
  "check-disable-defaults",
  "clippy",
  "wasm-tx-kernel",
  "wasm-evm-kernel",
  "wasm-preimage-installer",
  "test",
  "doc"
]
