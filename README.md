<!--
SPDX-FileCopyrightText: 2022-2023 TriliTech <contact@trili.tech>
SPDX-FileCopyrightText: 2022 Nomadic Labs <contact@nomadic-labs.com>
SPDX-FileCopyrightText: 2022 x0y0z0 <x0y0z0tn@gmail.com>

SPDX-License-Identifier: MIT
-->

# Tezos Smart Rollup Kernels

WIP Tx & Evm kernels, and SDK crates.

Published [docs](https://tezos.gitlab.io/kernel/doc/kernel/index.html).

## Setup
Either native setup or docker are supported.

### Native
To get started, install **rust** using the [rustup](https://rustup.rs/) tool. Rust version **1.66** is required.

You may also need to install `clang`. See the [Dockerfile] for the full list.

You should then be able to add the `wasm` target by running:
```shell
rustup target add wasm32-unknown-unknown
```

### MacOS - prerequisites

When using `OSX`, you will additionally need to run the following:
```shell
brew install llvm
```

In the output of this command, you should see the path that llvm is installed under:
```
If you need to have llvm first in your PATH, run:
 echo 'export PATH="/opt/homebrew/opt/llvm/bin:$PATH"' >> ~/.zshrc
```

You then need to add this to your path when building kernels. Otherwise, the `clang` picked-up 
by the build will not recognise the `wasm32-unknown-unknown` target.

```
export PATH=/usr/local/opt/llvm/bin:$PATH
```

### Dockerfile
Alternatively, development using docker can be enabled with:
```shell
source scripts/cargo-docker.sh
```

## Building
To build the crate, first install cargo make:
```shell
cargo install cargo-make
```

Then run the following to build the TX Kernel:
```shell
cargo make wasm-tx-kernel
```

> To build with signature verification disabled (possibly for tests) instead use `wasm-tx-kernel-no-sig-verif`.

You should now see `kernel_core.wasm` under `target/wasm32-unknown-unknown/release` or possibly
`target/wasm32-unknown-unknown/release/deps` depending on how you built things.

For other targets (e.g. evm & installer kernels, see [Makefile.toml](./Makefile.toml)).

# Test kernels
There are test kernels available, both for running computation, and for checking the behaviour of the pvm host functions.

Once built, you will also see a `test_kernel.wasm` under `target/wasm32-unknown-unknown/debug`. It will be under
`../release` if you add the `--release` flag to the cargo call above. To customize the test kernel,
use the feature flags listed in `test_kernel/Cargo.toml`. This is already set up as manual jobs with
Gitlab CI, so a range of test kernels can be build and downloaded from Gitlab.

The test kernels are un-stripped. To strip them and greatly reduce their size, use the `wasm-strip` utility
distributed with [The WebAssembly Binary Toolkit](https://github.com/WebAssembly/wabt). Run
```shell
wasm-strip test_kernel.wasm
```

## Running tests
To run tests for all crates, run:
```shell
cargo make test
```
