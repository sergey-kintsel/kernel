This folder contains test data for an external message sent fom DAC to L1 using Merkle Tree V1 pagination scheme. 
The content is an except from "Inferno, Canto I (Dante Alighieri)" and has a size of 5226 bytes. Test data was generated 
by passing `./original` as input to the `dac/store-preimage/` in a tezt simulation. The generated `./external_message`,
`pages/` and dac committee public key (in this case, 1 committee member found at `./signer`) were captured.