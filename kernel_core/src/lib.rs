// SPDX-FileCopyrightText: 2022-2023 TriliTech <contact@trili.tech>
// SPDX-FileCopyrightText: 2023 Marigold <contact@marigold.dev>
// SPDX-FileCopyrightText: 2022-2023 Nomadic Labs <contact@nomadic-labs.com>
//
// SPDX-License-Identifier: MIT

//! Contains core logic of the SCORU wasm kernel.
//!
//! The kernel is an implementation of a **TORU-style** *transactions* kernel.
#![deny(missing_docs)]
#![deny(rustdoc::all)]
#![forbid(unsafe_code)]

extern crate alloc;

pub mod bls;
pub mod deposit;
pub mod encoding;
pub mod inbox;
pub mod memory;
pub mod outbox;
pub mod storage;
pub mod transactions;

#[cfg(feature = "testing")]
pub(crate) mod fake_hash;

use crypto::hash::{ContractTz4Hash, HashTrait};
use deposit::DepositError;
use encoding::michelson::{MichelsonPair, MichelsonString};
use encoding::string_ticket::{StringTicket, StringTicketRepr};
use inbox::DepositFromInternalPayloadError;
use tezos_encoding::nom::error::DecodeError;
use thiserror::Error;

use debug::debug_msg;
use host::rollup_core::{
    RawRollupCore, MAX_INPUT_MESSAGE_SIZE, MAX_INPUT_SLOT_DATA_CHUNK_SIZE,
};
use host::runtime::Runtime;
use transactions::external_inbox::ProcessExtMsgError;

use crate::inbox::{InboxDeposit, InboxMessage, InternalInboxMessage, Transfer};
use crate::storage::{
    deposit_ticket_to_storage, init_account_storage, AccountStorage, AccountStorageError,
};
use crate::transactions::external_inbox;
use crate::transactions::withdrawal::process_withdrawals;

const MAX_READ_INPUT_SIZE: usize =
    if MAX_INPUT_MESSAGE_SIZE > MAX_INPUT_SLOT_DATA_CHUNK_SIZE {
        MAX_INPUT_MESSAGE_SIZE
    } else {
        MAX_INPUT_SLOT_DATA_CHUNK_SIZE
    };

impl TryFrom<MichelsonPair<MichelsonString, StringTicketRepr>> for InboxDeposit {
    type Error = DepositFromInternalPayloadError;

    fn try_from(
        value: MichelsonPair<MichelsonString, StringTicketRepr>,
    ) -> Result<Self, Self::Error> {
        Ok(Self {
            destination: ContractTz4Hash::from_b58check(value.0 .0.as_str())?,
            ticket: StringTicket::try_from(value.1)?,
        })
    }
}

/// Entrypoint of the *transactions* kernel.
pub fn transactions_run<Host>(host: &mut Host)
where
    Host: RawRollupCore,
{
    let mut account_storage = match init_account_storage() {
        Ok(v) => v,
        Err(err) => {
            debug_msg!(host; "Could not get account storage: {:?}", err);
            return;
        }
    };

    if let Ok(Some(message)) = host.read_input(MAX_READ_INPUT_SIZE) {
        debug_msg!(
            host;
            "Processing MessageData {} at level {}",
            message.id,
            message.level
        );

        if let Err(err) =
            process_inbox_message(host, &mut account_storage, message.as_ref())
        {
            debug_msg!(host; "Error processing header payload {}", err);
        }

        if host.mark_for_reboot().is_err() {
            debug_msg!(host; "Could not mark host for reboot");
        }
    }
}

#[derive(Error, Debug)]
enum TransactionError<'a> {
    #[error("unable to parse header inbox message {0}")]
    MalformedInboxMessage(nom::Err<DecodeError<&'a [u8]>>),
    #[error("Failed to deposit ticket: {0}")]
    Deposit(#[from] DepositError),
    #[error("Invalid internal inbox message, expected deposit: {0}")]
    InvalidInternalInbox(#[from] DepositFromInternalPayloadError),
    #[error("Error storing ticket on rollup")]
    StorageError(#[from] AccountStorageError),
    #[error("Failed to process external message: {0}")]
    ProcessExternalMsgError(#[from] ProcessExtMsgError),
}

fn process_inbox_message<'a, Host: RawRollupCore>(
    host: &mut Host,
    account_storage: &mut AccountStorage,
    inbox_message: &'a [u8],
) -> Result<(), TransactionError<'a>> {
    let (remaining, message) = InboxMessage::<
        MichelsonPair<MichelsonString, StringTicketRepr>,
    >::parse(inbox_message)
    .map_err(TransactionError::MalformedInboxMessage)?;

    match message {
        InboxMessage::Internal(InternalInboxMessage::Transfer(Transfer {
            payload,
            destination,
            ..
        })) => {
            let metadata = match Runtime::reveal_metadata(host) {
                Ok(a) => a,
                Err(e) => panic!("Failed to reveal_metadata: {:?}", e),
            };
            let rollup_address = metadata.address().unwrap();
            if rollup_address.0 != destination.hash().0 {
                debug_msg!(
                    host;
                    "Skipping message: Internal message targets another rollup. Expected: {}. Found: {}",
                    rollup_address,
                    destination.hash()
                );
            } else {
                let InboxDeposit {
                    destination,
                    ticket,
                } = payload
                    .try_into()
                    .map_err(TransactionError::InvalidInternalInbox)?;
                deposit_ticket_to_storage(host, account_storage, &destination, &ticket)?;
                debug_assert!(remaining.is_empty());
            }
            Ok(())
        }

        InboxMessage::Internal(
            msg @ (InternalInboxMessage::StartOfLevel
            | InternalInboxMessage::EndOfLevel
            | InternalInboxMessage::InfoPerLevel(..)),
        ) => {
            debug_msg!(host; "InboxMetadata: {}", msg);
            Ok(())
        }

        InboxMessage::External(message) => {
            external_inbox::process_external(host, account_storage, message)?
                .into_iter()
                .for_each(|withdrawals| process_withdrawals(host, withdrawals));
            Ok(())
        }
    }
}

/// Define the `kernel_run` for the transactions kernel.
#[cfg(feature = "tx-kernel")]
pub mod tx_kernel {
    use crate::transactions_run;
    use kernel::kernel_entry;
    kernel_entry!(transactions_run);
}

#[cfg(test)]
mod test {
    use crate::{
        bls::BlsKey,
        encoding::{
            contract::Contract,
            entrypoint::Entrypoint,
            michelson::{MichelsonContract, MichelsonPair, MichelsonString},
            public_key_hash::PublicKeyHash,
            smart_rollup::SmartRollupAddress,
            string_ticket::{StringTicket, StringTicketHash, StringTicketRepr},
        },
        inbox::{
            v1::{
                sendable::{Batch, Transaction},
                Operation, OperationContent,
            },
            ExternalInboxMessage, ParsedExternalInboxMessage, Signer,
        },
        storage::{account_path, Account},
    };
    use std::{fs, path::Path, str::FromStr};

    use super::*;

    use crypto::{
        base58::FromBase58Check,
        hash::{ContractKt1Hash, ContractTz4Hash, HashTrait},
    };
    use host::path::OwnedPath;
    use mock_runtime::{host::MockHost, state::HostState};
    use tezos_encoding::enc::BinWriter;
    use tezos_rollup_storage::storage::Storage;

    #[ignore]
    #[test]
    fn deposit_ticket() {
        // Arrange
        let mut mock_runtime = MockHost::default();

        let destination =
            ContractTz4Hash::from_b58check("tz4MSfZsn6kMDczShy8PMeB628TNukn9hi2K")
                .unwrap();

        let ticket_creator =
            Contract::from_b58check("KT1JW6PwhfaEJu6U3ENsxUeja48AdtqSoekd").unwrap();
        let ticket_content = "hello, ticket!";
        let ticket_quantity = 5;

        let message =
            InboxMessage::<MichelsonPair<MichelsonString, StringTicketRepr>>::Internal(
                InternalInboxMessage::Transfer(Transfer {
                    payload: MichelsonPair(
                        MichelsonString(destination.to_base58_check()),
                        MichelsonPair(
                            MichelsonContract(ticket_creator.clone()),
                            MichelsonPair(
                                MichelsonString(ticket_content.to_string()),
                                ticket_quantity.into(),
                            ),
                        ),
                    ),
                    sender: ContractKt1Hash::from_b58check(
                        "KT1VsSxSXUkgw6zkBGgUuDXXuJs9ToPqkrCg",
                    )
                    .unwrap(),
                    source: PublicKeyHash::from_b58check(
                        "tz3SvEa4tSowHC5iQ8Aw6DVKAAGqBPdyK1MH",
                    )
                    .unwrap(),
                    destination: SmartRollupAddress::new(
                        mock_runtime.as_mut().get_metadata().address().unwrap(),
                    ),
                }),
            );

        let mut input = Vec::new();
        message.bin_write(&mut input).unwrap();

        let level = 10;
        mock_runtime.as_mut().set_ready_for_input(level);
        mock_runtime
            .as_mut()
            .add_next_inputs(10, [input.as_slice()].into_iter());

        // Act
        transactions_run(&mut mock_runtime);

        // Assert
        // TODO use the accounts in durable storage instead
        /*
        let memory = Memory::load_memory(&mock_runtime);
        let account = memory
            .accounts()
            .account_of(&destination)
            .cloned()
            .expect("Account should be created");

        let mut expected_account = Account::default();
        let expected_tiket = StringTicket::new(
            ticket_creator,
            ticket_content.to_string(),
            ticket_quantity as u64,
        );
        expected_account
            .add_ticket(expected_ticket.identify().unwrap(), ticket_quantity as u64)
            .unwrap();

        assert_eq!(expected_account, account);
        */
    }

    #[ignore]
    #[test]
    fn deposit_fails_on_invalid_destination() {
        // Arrange
        let mut mock_runtime = MockHost::default();

        let destination = "tz4BAD";

        let ticket_creator =
            Contract::from_b58check("KT1JW6PwhfaEJu6U3ENsxUeja48AdtqSoekd").unwrap();
        let ticket_content = "hello, ticket!";
        let ticket_quantity = 5;

        let message =
            InboxMessage::<MichelsonPair<MichelsonString, StringTicketRepr>>::Internal(
                InternalInboxMessage::Transfer(Transfer {
                    payload: MichelsonPair(
                        MichelsonString(destination.to_string()),
                        MichelsonPair(
                            MichelsonContract(ticket_creator.clone()),
                            MichelsonPair(
                                MichelsonString(ticket_content.to_string()),
                                ticket_quantity.into(),
                            ),
                        ),
                    ),
                    sender: ContractKt1Hash::from_b58check(
                        "KT1VsSxSXUkgw6zkBGgUuDXXuJs9ToPqkrCg",
                    )
                    .unwrap(),
                    source: PublicKeyHash::from_b58check(
                        "tz3SvEa4tSowHC5iQ8Aw6DVKAAGqBPdyK1MH",
                    )
                    .unwrap(),
                    destination: SmartRollupAddress::new(
                        mock_runtime.as_mut().get_metadata().address().unwrap(),
                    ),
                }),
            );

        let mut input = Vec::new();
        message.bin_write(&mut input).unwrap();

        let level = 10;
        mock_runtime.as_mut().set_ready_for_input(level);
        mock_runtime
            .as_mut()
            .add_next_inputs(10, [input.as_slice()].into_iter());

        // Act
        transactions_run(&mut mock_runtime);

        // Assert
        // TODO use durable storage instead. We can only do this when the TX kernel has
        // a ticket balance sheet for the rollup node as a whole.
    }

    fn ticket(ticket: &StringTicket, override_amount: Option<u64>) -> StringTicket {
        let amount = override_amount.unwrap_or(ticket.amount);
        StringTicket {
            creator: ticket.creator.clone(),
            contents: ticket.contents.clone(),
            amount,
        }
    }

    #[test]
    fn ticket_txs_from_dac_external_message() {
        // Init state
        // deposit 100 A to acc1 pubkey
        // deposit 100 B to acc2 pubkey

        // TX1
        // 5 times (
        // acc1 transfers 10 A to acc2
        // acc2 transfers 10 A to acc3
        // )

        // TX2
        // 3 times (
        // acc2 transfers 10 B to acc1
        // acc2 transfers 10 B to acc3
        //)
        // withdraw 20 A from acc3
        // withdraw 20 B from acc3

        // acc1 - 50A 30B
        // acc2 - 0A 40B
        // acc3 - 30A 10B

        // initalize
        let ikm1 = [
            4, 150, 235, 215, 92, 7, 247, 205, 20, 153, 231, 78, 213, 201, 187, 3, 143,
            31, 201, 114, 103, 113, 140, 59, 133, 11, 32, 2, 222, 200, 163, 108,
        ];
        let ikm2 = [
            178, 115, 29, 156, 239, 203, 72, 33, 172, 178, 56, 97, 75, 138, 87, 134, 15,
            228, 229, 238, 251, 86, 226, 209, 192, 11, 9, 197, 152, 25, 182, 181,
        ];
        let ikm3 = [
            103, 231, 179, 62, 81, 102, 142, 116, 245, 81, 92, 2, 252, 4, 229, 69, 188,
            72, 109, 13, 161, 74, 64, 30, 143, 59, 104, 71, 56, 207, 48, 136,
        ];

        let acc1 = BlsKey::from_ikm(ikm1);
        let acc2 = BlsKey::from_ikm(ikm2);
        let acc3 = BlsKey::from_ikm(ikm3);

        let acc1 = (&acc1, account_path(acc1.public_key_hash()).unwrap());
        let acc2 = (&acc2, account_path(acc2.public_key_hash()).unwrap());
        let acc3 = (&acc3, account_path(acc3.public_key_hash()).unwrap());

        let ticket_a = StringTicket {
            creator: Contract::Originated(
                ContractKt1Hash::from_str("KT1Gow8VzXZx3Akn5kvjACqnjnyYBxQpzSKr")
                    .unwrap(),
            ),
            contents: "A".to_string(),
            amount: 0,
        };
        let ticket_b = StringTicket {
            creator: Contract::Originated(
                ContractKt1Hash::from_str("KT1FAKEFAKEFAKEFAKEFAKEFAKEFAKGGSE2x")
                    .unwrap(),
            ),
            contents: "B".to_string(),
            amount: 0,
        };

        let mut state = HostState::default();
        let resource_dir = Path::new(env!("CARGO_MANIFEST_DIR"))
            .join("resources/dac_message/generated_tx");
        let signer_pk = fs::read_to_string(resource_dir.join("signer")).unwrap();
        let signer_pk: [u8; 48] = signer_pk.from_base58check().unwrap()[4..]
            .try_into()
            .unwrap();
        let rollup_id = 2u16;
        state.handle_store_write(b"/kernel/dac.committee/0", 0, &signer_pk);
        state.handle_store_write(b"/kernel/id", 0, &u16::to_le_bytes(rollup_id));
        let all_pages = fs::read_dir(resource_dir.join("pages")).unwrap();
        for page in all_pages {
            let page = page.unwrap();
            let page = fs::read(page.path()).unwrap();
            let preimage = page.to_vec();
            state.set_preimage(preimage);
        }

        let mut host = MockHost::from(state.clone());
        let account_storage = init_account_storage().unwrap();

        init_account_state(
            &account_storage,
            &mut host,
            &acc1.1,
            &acc2.1,
            &ticket_a.identify().unwrap(),
            &ticket_b.identify().unwrap(),
        );

        let withdraw_to_contract =
            ContractKt1Hash::from_str("KT1SL6CGhjPUyLypDbFv9bXsNF2sHG7Fy3j9").unwrap();
        let batch = make_transactions(
            &ticket_a,
            &ticket_b,
            &acc1,
            &acc2,
            &acc3,
            &withdraw_to_contract,
        );

        // Write
        let mut original_content = vec![0u8];
        batch.bin_write(&mut original_content).unwrap();

        // let resource_dir = Path::new(env!("CARGO_MANIFEST_DIR")).join("resources/dac_message/generated_tx/original/rollup2");
        // fs::write(resource_dir, original_content).unwrap();

        // Assert content in rollup2 is equivalent to hardcoded transactions above
        let external_message = fs::read(resource_dir.join("external_message"))
            .map(hex::decode)
            .unwrap()
            .unwrap();

        // parse message
        if let (_, ParsedExternalInboxMessage::DAC(parsed_dac_message)) =
            ParsedExternalInboxMessage::parse(&external_message).unwrap()
        {
            let revealed_content = parsed_dac_message
                .reveal_dac_message_for_rollup(&mut host, rollup_id)
                .unwrap();

            assert_eq!(original_content, revealed_content);
            let state = state.clone();
            let mut host = MockHost::from(state);
            let account_storage = init_account_storage().unwrap();
            // Init state
            init_account_state(
                &account_storage,
                &mut host,
                &acc1.1,
                &acc2.1,
                &ticket_a.identify().unwrap(),
                &ticket_b.identify().unwrap(),
            );
            let mut message = vec![];
            InboxMessage::<MichelsonPair<MichelsonString, StringTicketRepr>>::External(
                ExternalInboxMessage(&original_content.as_slice()),
            )
            .bin_write(&mut message)
            .unwrap();
            let inputs = [message.as_slice()];
            let level = 10;

            host.as_mut().set_ready_for_input(level);
            host.as_mut().add_next_inputs(10, inputs.into_iter());

            transactions_run(&mut host);
            assert_state(
                &host,
                &acc1,
                &acc2,
                &acc3,
                &account_storage,
                &ticket_a.identify().unwrap(),
                &ticket_b.identify().unwrap(),
            );
        } else {
            panic!("External inbox message expected to be a DAC external message");
        }
        // Test
        let mut message = vec![];
        InboxMessage::<MichelsonPair<MichelsonString, StringTicketRepr>>::External(
            ExternalInboxMessage(external_message.as_slice()),
        )
        .bin_write(&mut message)
        .unwrap();
        let inputs = [message.as_slice()];
        let level = 10;

        host.as_mut().set_ready_for_input(level);
        host.as_mut().add_next_inputs(10, inputs.into_iter());

        transactions_run(&mut host);

        assert_state(
            &host,
            &acc1,
            &acc2,
            &acc3,
            &account_storage,
            &ticket_a.identify().unwrap(),
            &ticket_b.identify().unwrap(),
        );

        fn assert_state<Host: Runtime + RawRollupCore>(
            host: &Host,
            acc1: &(&BlsKey, OwnedPath),
            acc2: &(&BlsKey, OwnedPath),
            acc3: &(&BlsKey, OwnedPath),
            account_storage: &Storage<Account>,
            ticket_a: &StringTicketHash,
            ticket_b: &StringTicketHash,
        ) {
            let account1 = get_account_unsafe(host, &acc1.1, &account_storage);
            let account2 = get_account_unsafe(host, &acc2.1, &account_storage);
            let account3 = get_account_unsafe(host, &acc3.1, &account_storage);
            let acc_1_a = get_ticket_unsafe(&account1, host, ticket_a);
            let acc_1_b = get_ticket_unsafe(&account1, host, ticket_b);
            let acc_2_a = get_ticket_unsafe(&account2, host, ticket_a);
            let acc_2_b = get_ticket_unsafe(&account2, host, ticket_b);
            let acc_3_a = get_ticket_unsafe(&account3, host, ticket_a);
            let acc_3_b = get_ticket_unsafe(&account3, host, ticket_b);

            assert_eq!(50, acc_1_a);
            assert_eq!(30, acc_1_b);
            assert_eq!(0, acc_2_a);
            assert_eq!(40, acc_2_b);
            assert_eq!(30, acc_3_a);
            assert_eq!(10, acc_3_b);
        }

        fn make_transactions(
            ticket_a: &StringTicket,
            ticket_b: &StringTicket,
            acc1: &(&BlsKey, OwnedPath),
            acc2: &(&BlsKey, OwnedPath),
            acc3: &(&BlsKey, OwnedPath),
            withdraw_to_contract: &ContractKt1Hash,
        ) -> Batch {
            // TX1
            let acc1_contents = vec![
                OperationContent::transfer(
                    acc2.0.public_key_hash().clone(),
                    ticket(&ticket_a, Some(10)),
                ),
                OperationContent::transfer(
                    acc2.0.public_key_hash().clone(),
                    ticket(&ticket_a, Some(10)),
                ),
                OperationContent::transfer(
                    acc2.0.public_key_hash().clone(),
                    ticket(&ticket_a, Some(10)),
                ),
                OperationContent::transfer(
                    acc2.0.public_key_hash().clone(),
                    ticket(&ticket_a, Some(10)),
                ),
                OperationContent::transfer(
                    acc2.0.public_key_hash().clone(),
                    ticket(&ticket_a, Some(10)),
                ),
            ];
            let acc1_operation = Operation {
                signer: Signer::BlsPublicKey(acc1.0.compressed_public_key().clone()),
                counter: 0,
                contents: Vec::from(acc1_contents),
            };

            let acc2_contents = vec![
                OperationContent::transfer(
                    acc3.0.public_key_hash().clone(),
                    ticket(&ticket_a, Some(10)),
                ),
                OperationContent::transfer(
                    acc3.0.public_key_hash().clone(),
                    ticket(&ticket_a, Some(10)),
                ),
                OperationContent::transfer(
                    acc3.0.public_key_hash().clone(),
                    ticket(&ticket_a, Some(10)),
                ),
                OperationContent::transfer(
                    acc3.0.public_key_hash().clone(),
                    ticket(&ticket_a, Some(10)),
                ),
                OperationContent::transfer(
                    acc3.0.public_key_hash().clone(),
                    ticket(&ticket_a, Some(10)),
                ),
            ];

            let acc2_operation = Operation {
                signer: Signer::BlsPublicKey(acc2.0.compressed_public_key().clone()),
                counter: 0,
                contents: acc2_contents,
            };

            let tx1 = Transaction::new(vec![
                (acc1_operation, acc1.0.clone()),
                (acc2_operation, acc2.0.clone()),
            ])
            .unwrap();

            // TX2
            let acc2_contents = vec![
                OperationContent::transfer(
                    acc1.0.public_key_hash().clone(),
                    ticket(&ticket_b, Some(10)),
                ),
                OperationContent::transfer(
                    acc1.0.public_key_hash().clone(),
                    ticket(&ticket_b, Some(10)),
                ),
                OperationContent::transfer(
                    acc1.0.public_key_hash().clone(),
                    ticket(&ticket_b, Some(10)),
                ),
                OperationContent::transfer(
                    acc3.0.public_key_hash().clone(),
                    ticket(&ticket_b, Some(10)),
                ),
                OperationContent::transfer(
                    acc3.0.public_key_hash().clone(),
                    ticket(&ticket_b, Some(10)),
                ),
                OperationContent::transfer(
                    acc3.0.public_key_hash().clone(),
                    ticket(&ticket_b, Some(10)),
                ),
            ];

            let acc2_operation = Operation {
                signer: Signer::Layer2Address(acc2.0.public_key_hash().clone()),
                counter: 1,
                contents: acc2_contents,
            };

            let acc3_contents = vec![
                OperationContent::withdrawal(
                    withdraw_to_contract.clone(),
                    ticket(&ticket_a, Some(20)),
                    Entrypoint::default(),
                ),
                OperationContent::withdrawal(
                    withdraw_to_contract.clone(),
                    ticket(&ticket_b, Some(20)),
                    Entrypoint::default(),
                ),
            ];

            let acc3_operation = Operation {
                signer: Signer::BlsPublicKey(acc3.0.compressed_public_key().clone()),
                counter: 0,
                contents: acc3_contents,
            };

            let tx2 = Transaction::new(vec![
                (acc2_operation, acc2.0.clone()),
                (acc3_operation, acc3.0.clone()),
            ])
            .unwrap();

            Batch::new(vec![tx1, tx2])
        }

        fn init_account_state<Host: Runtime + RawRollupCore>(
            account_storage: &Storage<Account>,
            host: &mut Host,
            acc1: &OwnedPath,
            acc2: &OwnedPath,
            ticket_a: &StringTicketHash,
            ticket_b: &StringTicketHash,
        ) -> (Account, Account) {
            // Init state
            let mut account1 = account_storage.get_or_create_account(host, acc1).unwrap();
            account1.add_ticket(host, ticket_a, 100).unwrap();
            let mut account2 = account_storage.get_or_create_account(host, acc2).unwrap();
            account2.add_ticket(host, ticket_b, 100).unwrap();
            (account1, account2)
        }
    }

    fn get_account_unsafe(
        host: &impl Runtime,
        id: &impl host::path::Path,
        account_storage: &Storage<Account>,
    ) -> Account {
        account_storage.get_account(host, id).unwrap().unwrap()
    }

    fn get_ticket_unsafe<Host: Runtime + RawRollupCore>(
        account: &Account,
        host: &Host,
        ticket_hash: &StringTicketHash,
    ) -> u64 {
        account
            .ticket_amount(host, ticket_hash)
            .unwrap()
            .unwrap_or(0)
    }

    #[test]
    fn test_filter_internal_transfers() {
        // setup rollup state
        let state = HostState::default();
        let mut host = MockHost::from(state);
        let metadata = Runtime::reveal_metadata(&host).unwrap();
        // setup message
        let bls_key = BlsKey::from_ikm([1; 32]);
        let receiver = bls_key.public_key_hash().clone();
        let originator = Contract::Originated(
            ContractKt1Hash::from_b58check("KT1ThEdxfUcWUwqsdergy3QnbCWGHSUHeHJq")
                .unwrap(),
        );
        let contents = "Hello, Ticket!".to_string();
        let string_ticket = StringTicket::new(originator.clone(), contents.clone(), 500);
        let sender =
            ContractKt1Hash::from_b58check("KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn")
                .unwrap();
        let source =
            PublicKeyHash::from_b58check("tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU").unwrap();
        // Transfers
        // ignore transfer 100
        let transfer1 =
            InboxMessage::Internal(InternalInboxMessage::Transfer(Transfer {
                payload: MichelsonPair::<MichelsonString, StringTicketRepr>(
                    MichelsonString(receiver.to_b58check()),
                    StringTicket::new(originator.clone(), contents.clone(), 100).into(),
                ),
                sender: sender.clone(),
                source: source.clone(),
                destination: SmartRollupAddress::from_b58check(
                    "sr1VEw81u5kYf8nJ3cwqgVEaVRiMZMixueFJ",
                )
                .unwrap(),
            }));
        let mut message1 = Vec::new();
        transfer1.bin_write(&mut message1).unwrap();

        // apply transfer 500
        let transfer2 =
            InboxMessage::Internal(InternalInboxMessage::Transfer(Transfer {
                payload: MichelsonPair::<MichelsonString, StringTicketRepr>(
                    MichelsonString(receiver.to_b58check()),
                    string_ticket.testing_clone().into(),
                ),
                sender: sender.clone(),
                source,
                destination: SmartRollupAddress::new(metadata.address().unwrap()),
            }));
        let mut message2 = Vec::new();
        transfer2.bin_write(&mut message2).unwrap();

        let messages = [message1.as_slice(), &message2];

        host.as_mut().set_ready_for_input(10);
        host.as_mut().add_next_inputs(10, messages.into_iter());

        // Act
        transactions_run(&mut host);
        transactions_run(&mut host);

        let account_storage = init_account_storage().unwrap();
        let receiver_acc = get_account_unsafe(
            &host,
            &account_path(&receiver).unwrap(),
            &account_storage,
        );
        let ticket_amount =
            get_ticket_unsafe(&receiver_acc, &host, &string_ticket.identify().unwrap());

        assert_eq!(500, ticket_amount);
    }
}
