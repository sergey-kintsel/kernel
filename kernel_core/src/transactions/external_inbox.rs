// SPDX-FileCopyrightText: 2022-2023 TriliTech <contact@trili.tech>
//
// SPDX-License-Identifier: MIT

//! Processing external inbox messages - withdrawals & transactions.

use crate::bls::BlsError;
use crate::bls::PublicKey;
use crate::inbox::dac_message::RevealDacMessageError;
use crate::inbox::v1::verifiable::VerificationError;
use crate::inbox::v1::verifiable::VerifiedTransaction;
use crate::inbox::v1::ParsedBatch;
use crate::inbox::ExternalInboxMessage;
use crate::inbox::ParsedExternalInboxMessage;
use crate::storage::AccountStorage;
use crate::transactions::withdrawal::Withdrawal;
use debug::debug_msg;
use host::path::RefPath;
use host::rollup_core::RawRollupCore;
use host::runtime::RuntimeError;
use thiserror::Error;

pub(crate) const KERNEL_ID_PATH: RefPath = RefPath::assert_from(b"/kernel/id");
// TODO: replace with `dac_committee`, `_-` now allowed in paths by PVM,
//       but `host::path` hasn't been updated yet.
pub(crate) const DAC_COMMITTEE_MEMBER_PATH: RefPath =
    RefPath::assert_from(b"/kernel/dac.committee/0");

/// Possible outcomes when processing messages
pub enum ProcessedOutcome {
    /// A verified transaction
    Transaction(VerifiedTransaction),
    /// Call again for the next transaction
    More,
    /// Finished processing transactions
    Finished,
}

/// Process external message using durable storage directly
pub fn process_external<'a, Host: RawRollupCore>(
    host: &mut Host,
    account_storage: &mut AccountStorage,
    message: ExternalInboxMessage<'a>,
) -> Result<Vec<Vec<Withdrawal>>, ProcessExtMsgError> {
    match parse_external::<Host>(message) {
        Some(ParsedExternalInboxMessage::V1(batch)) => {
            Ok(process_batch_message(host, account_storage, batch))
        }
        None => Err(ProcessExtMsgError::ParseError),
        Some(ParsedExternalInboxMessage::DAC(parsed_dac_message)) => {
            let (dac_committee, rollup_id) = dac_details(host)?;
            let dac_committee = [&dac_committee];
            parsed_dac_message.verify_signature(&dac_committee)?;
            let result_buf =
                parsed_dac_message.reveal_dac_message_for_rollup(host, rollup_id)?;
            let message = ExternalInboxMessage(&result_buf);
            let external_message = parse_external::<Host>(message);
            match external_message {
                Some(ParsedExternalInboxMessage::V1(batch)) => {
                    Ok(process_batch_message(host, account_storage, batch))
                }
                None => Err(ProcessExtMsgError::ParseError),
                _ => {
                    debug_msg!(
                        host;
                        "Expected DAC message to contain ParsedExternalInboxMessage::V1"
                    );
                    Err(ProcessExtMsgError::UnexpectedExtMsgVariant)
                }
            }
        }
    }
}

fn process_batch_message<Host: RawRollupCore>(
    host: &mut Host,
    account_storage: &mut AccountStorage,
    batch: ParsedBatch,
) -> Vec<Vec<Withdrawal>> {
    let mut all_withdrawals: Vec<Vec<Withdrawal>> = Vec::new();
    if let Err(e) = batch.verify_signature_durable(host, account_storage) {
        debug_msg!(host; "Signature verification of batch failed: {}", e);
        all_withdrawals
    } else {
        for transaction in batch.transactions.into_iter() {
            if account_storage.begin_transaction(host).is_err() {
                debug_msg!(host; "Failed to begin transaction");
                continue;
            }

            match transaction.execute(host, account_storage) {
                Ok(withdrawals) => {
                    if account_storage.commit(host).is_err() {
                        panic!("Failed to commit transaction");
                    }

                    all_withdrawals.push(withdrawals);
                }
                Err(err) => {
                    debug_msg!(host; "Could not execute transaction: {}", err);
                    if account_storage.rollback(host).is_err() {
                        panic!("Failed to rollback transaction");
                    }
                }
            };
        }

        all_withdrawals
    }
}

fn dac_details<Host: RawRollupCore>(
    host: &Host,
) -> Result<(PublicKey, u16), ProcessExtMsgError> {
    let dac_committee = get_dac_committee(host)?;
    let rollup_id = get_rollup_id(host)?;
    Ok((dac_committee, rollup_id))
}

fn get_rollup_id<Host: RawRollupCore>(host: &Host) -> Result<u16, ProcessExtMsgError> {
    let mut id = [0; 2];
    host::runtime::load_value_slice(host, &KERNEL_ID_PATH, &mut id)
        .map(|_| u16::from_le_bytes(id))
        .map_err(ProcessExtMsgError::LoadRollupIdError)
}

fn get_dac_committee<Host: RawRollupCore>(
    host: &Host,
) -> Result<PublicKey, ProcessExtMsgError> {
    // Assume that we have 1 dac committee member pk stored as raw bytes of CompressedPK.
    // In the future, we probably want to have a bigger dac committee.
    let mut dac_member = [0; 48];
    host::runtime::load_value_slice(host, &DAC_COMMITTEE_MEMBER_PATH, &mut dac_member)
        .map_err(LoadDacCommitteeError::RuntimeError)?;
    let pk = PublicKey::try_from(dac_member).map_err(LoadDacCommitteeError::BlsError)?;
    Ok(pk)
}

/// Parse external message, logging error if it occurs.
fn parse_external<Host: RawRollupCore>(
    message: ExternalInboxMessage,
) -> Option<ParsedExternalInboxMessage> {
    match ParsedExternalInboxMessage::parse(message.0) {
        Ok((remaining, external)) => {
            if !remaining.is_empty() {
                debug_msg!(
                    Host,
                    "External message had unused remaining bytes: {:?}",
                    remaining
                );
            }
            Some(external)
        }
        Err(err) => {
            debug_msg!(Host, "Error parsing external message payload {}", err);
            None
        }
    }
}

/// Errors when processing external messages
#[derive(Error, Debug)]
pub enum ProcessExtMsgError {
    /// Parse Error
    #[error("Error parsing external message")]
    ParseError,

    /// Propogate [VerificationError] in the context of dac agg sig verificaiton
    #[error("Dac aggregate signature verification failed: {0}")]
    DacAggregateSigVerficationError(#[from] VerificationError),

    /// Errors when loading roll up id from storage
    #[error("Failed to load rollup id: {0:?}")]
    LoadRollupIdError(RuntimeError),

    /// Errors when loading dac committee from storage
    #[error("Failed to load dac committee: {0}")]
    LoadDacCommitteeError(#[from] LoadDacCommitteeError),

    /// Propogate errors from [RevealDacMessageError]
    #[error(transparent)]
    RevealDacMessageError(#[from] RevealDacMessageError),

    /// External message was the wrong variant
    #[error("Unexpected external message variant")]
    UnexpectedExtMsgVariant,
}
/// LoadDacCommitteeError variants
#[derive(Error, Debug)]
pub enum LoadDacCommitteeError {
    /// RuntimeError from host
    #[error("{0:?}")]
    RuntimeError(RuntimeError),

    /// Bls errors from decode
    #[error(transparent)]
    BlsError(#[from] BlsError),
}
