// SPDX-FileCopyrightText: 2022-2023 TriliTech <contact@trili.tech>
// SPDX-FileCopyrightText: 2022-2023 Nomadic Labs <contact@nomadic-labs.com>
//
// SPDX-License-Identifier: MIT

//! Types & encodings for the *inbox-half* of the *L1/L2 communication protocol*
//!
//! In *general*, this module is a re-implementation of the tezos-protocol
//! [inbox message repr].
//!
//! [inbox message repr]: <https://gitlab.com/tezos/tezos/-/blob/9028b797894a5d9db38bc61a20abb793c3778316/src/proto_alpha/lib_protocol/sc_rollup_inbox_message_repr.mli>

use crypto::base58::FromBase58CheckError;
use crypto::hash::{BlockHash, ContractKt1Hash, ContractTz4Hash};
use nom::combinator::{map, rest};
use std::fmt::Display;
use tezos_encoding::enc::BinWriter;
use tezos_encoding::encoding::HasEncoding;
use tezos_encoding::nom::NomReader;
use tezos_rollup_encoding::timestamp::Timestamp;
use thiserror::Error;

use crate::encoding::michelson::Michelson;
use crate::encoding::public_key_hash::PublicKeyHash;
use crate::encoding::smart_rollup::SmartRollupAddress;
use crate::encoding::string_ticket::{StringTicket, StringTicketError};

pub mod external;
pub mod sendable;

pub use self::external::*;

#[derive(Debug, PartialEq, Eq, NomReader, HasEncoding)]
enum InboxMessageRepr<Expr: Michelson> {
    #[encoding(tag = 0)]
    Internal(InternalInboxMessage<Expr>),
    #[encoding(tag = 1)]
    External,
}

/// Inbox message, received by the kernel as tezos-encoded bytes.
#[derive(Debug, PartialEq, Eq, BinWriter)]
pub enum InboxMessage<'a, Expr: Michelson> {
    /// Message sent from an L1 smart-contract.
    Internal(InternalInboxMessage<Expr>),
    /// Message of arbitrary bytes, in a format specific to the kernel.
    ///
    /// The containing operation will be sent by an implicit account - but will
    /// deserialize to a structure representing *transactions* & *withdrawals* between
    /// and from **Layer2Tz4* addresses respectively.
    External(ExternalInboxMessage<'a>),
}

impl<'a, Expr: Michelson> InboxMessage<'a, Expr> {
    /// Replacement for `nom_read` for [InboxMessage].
    ///
    /// [NomReader] trait unfortunately does not propagate lifetime of the input bytes,
    /// meaning that it is impossible to use it with a type that refers to a section of
    /// the input.
    ///
    /// In our case, we want to avoid copies if possible - which require additional ticks.
    pub fn parse(input: &'a [u8]) -> tezos_encoding::nom::NomResult<Self> {
        let (remaining, repr): (&'a [u8], _) = InboxMessageRepr::nom_read(input)?;

        match repr {
            InboxMessageRepr::Internal(i) => Ok((remaining, InboxMessage::Internal(i))),
            InboxMessageRepr::External => map(rest, |ext| {
                InboxMessage::External(ExternalInboxMessage(ext))
            })(remaining),
        }
    }
}

#[derive(Debug, PartialEq, Eq, NomReader, HasEncoding, BinWriter)]
/// Transfer sent by an L1 smart-contract.
pub struct Transfer<Expr: Michelson> {
    /// Micheline-encoded payload, sent by the calling contract.
    pub payload: Expr,
    /// The calling smart-contract.
    pub sender: ContractKt1Hash,
    /// The originator of the transaction.
    pub source: PublicKeyHash,
    /// The destination of the message.
    pub destination: SmartRollupAddress,
}

impl<E: Michelson> Display for Transfer<E> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "Transfer {{sender: {}, source: {}, dest: {}}}",
            self.sender, self.source, self.destination
        )
    }
}

/// Metainformation per inbox level, usually goes after StartOfLevel message
#[derive(Debug, PartialEq, Eq, NomReader, HasEncoding, BinWriter)]
pub struct InfoPerLevel {
    /// Timestamp of predecessor block
    pub predecessor_timestamp: Timestamp,
    /// Hash of predecessor block
    pub predecessor: BlockHash,
}

impl Display for InfoPerLevel {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "InfoPerLevel {{predecessor_timestamp: {}, predecessor: {}}}",
            self.predecessor_timestamp, self.predecessor
        )
    }
}

/// Internal inbox message - known to be sent by the protocol
#[derive(Debug, PartialEq, Eq, NomReader, HasEncoding, BinWriter)]
pub enum InternalInboxMessage<Expr: Michelson> {
    /// Transfer message
    Transfer(Transfer<Expr>),
    /// Start of level message, pushed at the beginning of an inbox level.
    StartOfLevel,
    /// End of level message, pushed at the end of an inbox level.
    EndOfLevel,
    /// Info per level, goes after StartOfLevel
    InfoPerLevel(InfoPerLevel),
}

impl<Expr: Michelson> Display for InternalInboxMessage<Expr> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Transfer(tr) => write!(f, "{}", tr),
            Self::StartOfLevel => write!(f, "StartOfLevel"),
            Self::EndOfLevel => write!(f, "EndOfLevel"),
            Self::InfoPerLevel(ipl) => write!(f, "{}", ipl),
        }
    }
}

/// Representation of a string-ticket deposit into the rollup.
#[derive(Debug, PartialEq, Eq)]
pub struct InboxDeposit {
    /// The destination account of the deposit.
    pub destination: ContractTz4Hash,
    /// The ticket - including amount - to be deposited.
    pub ticket: StringTicket,
}

/// Error converting from a Michelson payload to [InboxDeposit].
#[derive(Debug, Error)]
pub enum DepositFromInternalPayloadError {
    /// Invalid destination
    #[error("Invalid Layer2 address {0}")]
    InvalidDestination(#[from] FromBase58CheckError),
    /// Invalid Ticket repr
    #[error("Invalid ticket representation {0}")]
    InvalidTicket(#[from] StringTicketError),
}

#[cfg(test)]
mod test {
    use crypto::hash::HashTrait;

    use crate::encoding::{
        contract::Contract,
        michelson::{MichelsonPair, MichelsonString},
        string_ticket::StringTicketRepr,
    };

    // Ports of *internal_inbox_message* tests from
    // <https://gitlab.com/tezos/tezos/-/blob/a7a99d13b19121bfc9353426b545c3fb7f91da28/src/proto_alpha/lib_protocol/test/unit/test_sc_rollup_management_protocol.ml>
    //
    // Needed to ensure binary-compatability with messages from Layer 1.
    use super::*;

    type Expr = MichelsonPair<MichelsonString, StringTicketRepr>;

    fn test_encode_decode(expected_bytes: Vec<u8>, inbox_message: InboxMessage<Expr>) {
        // Encoding
        let mut bin = Vec::new();
        inbox_message
            .bin_write(&mut bin)
            .expect("serialization should succeed");

        assert_eq!(expected_bytes, bin, "error in serialization");

        // Decoding
        let (input_remaining, parsed_message) =
            InboxMessage::parse(bin.as_slice()).expect("deserialization should work");

        assert!(input_remaining.is_empty());
        assert_eq!(inbox_message, parsed_message, "error in deserialization");
    }

    #[test]
    fn test_encode_decode_internal_transfer() {
        // binary encoding produced by lightly-modified (to print encoded data) protocol test
        // representing a `Pair ( string, string ticket )`
        let expected_bytes = vec![
            // Inbox message start
            0, // Internal tag
            0, // Transfer tag
            // Payload
            7, // Prim_2
            7, // Pair tag,
            1, // String tag
            0, 0, 0, b'$', // String size
            b't', b'z', b'4', b'M', b'S', b'f', b'Z', b's', b'n', b'6',
            b'k', // tz4 (1)
            b'M', b'D', b'c', b'z', b'S', b'h', b'y', b'8', b'P', b'M',
            b'e', // tz4 (2)
            b'B', b'6', b'2', b'8', b'T', b'N', b'u', b'k', b'n', b'9',
            b'h', // tz4 (3)
            b'i', b'2', b'K',  // tz3 (4)
            7,     // Prim_2
            7,     // Pair tag
            b'\n', // Bytes tag
            0, 0, 0, 22, // Bytes length
            // KT1ThEdxfUcWUwqsdergy3QnbCWGHSUHeHJq
            1, 209, 163, b'|', 8, 138, 18, b'!', 182, b'6', 187, b'_', 204, 179, b'^', 5,
            24, 16, b'8', 186, b'|', // kt1 end
            0,    // Padding
            7,    // Prim_2
            7,    // Pair
            1,    // String tag
            0, 0, 0, 3, // String size
            b'r', b'e', b'd', // string contents
            0,    // int encoding tag
            1,    // amount
            // Sender KT1BuEZtb68c1Q4yjtckcNjGELqWt56Xyesc - contract hash
            b'$', b'f', b'g', 169, b'1', 254, 11, 210, 251, 028, 182, 4, 247, 020, b'`',
            30, 136, b'(', b'E', b'P', // end kt1
            // Source tz1RjtZUVeLhADFHDL8UwDZA6vjWWhojpu5w
            0, // PKH - Ed25519 tag
            b'B', 236, b'v', b'_', b'\'', 0, 019, b'N', 158, 14, 254, 137, 208, b'3',
            142, b'.', 132, b'<', b'S', 220, // end tz1
            // Destination sr1UX4Euo29Fd5bhmZypQffZJwy9M3A1D4Mb
            246, 144, 126, 197, 72, 77, 70, 203, 171, 146, 47, 210, 213, 225, 165, 143,
            212, 162, 185, 251, // end sr1
        ];

        let l2_address = "tz4MSfZsn6kMDczShy8PMeB628TNukn9hi2K".into();

        let ticket_creator =
            Contract::from_b58check("KT1ThEdxfUcWUwqsdergy3QnbCWGHSUHeHJq")
                .expect("valid Kt1 address");

        let string_ticket = StringTicket::new(ticket_creator, "red".into(), 1);

        let sender =
            ContractKt1Hash::from_b58check("KT1BuEZtb68c1Q4yjtckcNjGELqWt56Xyesc")
                .expect("valid Kt1 address");

        let source = PublicKeyHash::from_b58check("tz1RjtZUVeLhADFHDL8UwDZA6vjWWhojpu5w")
            .expect("valid tz1 address");

        let destination =
            SmartRollupAddress::from_b58check("sr1UX4Euo29Fd5bhmZypQffZJwy9M3A1D4Mb")
                .expect("valid sr1 address");

        let inbox_message =
            InboxMessage::Internal(InternalInboxMessage::Transfer(Transfer {
                payload: MichelsonPair(MichelsonString(l2_address), string_ticket.into()),
                sender,
                source,
                destination,
            }));

        test_encode_decode(expected_bytes, inbox_message)
    }

    #[test]
    fn test_encode_decode_sol() {
        // binary encoding produced by lightly-modified (to print encoded data) protocol test
        let expected_bytes = vec![
            // Inbox message start
            0, // Internal tag
            1, // Start of level tag
        ];

        let inbox_message = InboxMessage::Internal(InternalInboxMessage::StartOfLevel);

        test_encode_decode(expected_bytes, inbox_message)
    }

    #[test]
    fn test_encode_decode_eol() {
        // binary encoding produced by lightly-modified (to print encoded data) protocol test
        let expected_bytes = vec![
            // Inbox message start
            0, // Internal tag
            2, // End of level tag
        ];

        let inbox_message = InboxMessage::Internal(InternalInboxMessage::EndOfLevel);

        test_encode_decode(expected_bytes, inbox_message)
    }

    #[test]
    fn test_encode_decode_external_inbox_message() {
        // The prefix consists of 1 bytes:
        // - Byte 0 is the tag (0 for internal, 1 for external).
        let assert_enc_with_prefix = |message: Vec<u8>| {
            let inbox_message =
                InboxMessage::External(ExternalInboxMessage(message.as_slice()));

            let mut real_encoding = Vec::new();
            assert!(inbox_message.bin_write(&mut real_encoding).is_ok());

            let real_prefix = real_encoding[0];

            let mut expected_encoding = vec![1];
            expected_encoding.extend_from_slice(message.as_slice());

            assert_encode_decode_inbox_message(inbox_message);
            assert_eq!(1_u8, real_prefix);
            assert_eq!(expected_encoding, real_encoding);
        };

        assert_enc_with_prefix(vec![]);
        assert_enc_with_prefix(vec![b'A']);
        assert_enc_with_prefix("0123456789".as_bytes().to_vec());
        assert_enc_with_prefix(vec![b'B'; 256]);
        assert_enc_with_prefix(vec![b'\n'; 1234567]);
        // Contents should not impact the prefix
        assert_enc_with_prefix(vec![5; 1234567]);
    }

    fn assert_encode_decode_inbox_message(
        message: InboxMessage<MichelsonPair<MichelsonString, StringTicketRepr>>,
    ) {
        let mut encoded = Vec::new();
        assert!(message.bin_write(&mut encoded).is_ok());

        let decoded = InboxMessage::parse(encoded.as_slice())
            .expect("Deserialization failed")
            .1;

        assert_eq!(message, decoded);
    }
}
