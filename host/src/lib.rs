// SPDX-FileCopyrightText: 2022-2023 TriliTech <contact@trili.tech>
// SPDX-FileCopyrightText: 2022-2023 Marigold <contact@marigold.dev>
//
// SPDX-License-Identifier: MIT

//! Definition of the SCORU wasm host functions.
//!
//! The host exposes 'safe capabilities' as a set of **C-style APIs**.  The `host`
//! crate defines these as `extern` functions (see [rollup_core]) and is
//! responsible for providing safe wrappers which can be called from **safe rust**.
#![cfg_attr(not(feature = "testing"), no_std)]
#![deny(missing_docs)]
#![deny(rustdoc::all)]

#[cfg(feature = "alloc")]
extern crate alloc;

pub mod input;
pub mod metadata;
pub mod path;
pub mod runtime;

#[deprecated(note = "use tezos_smart_rollup_core directly")]
/// DEPRECATED: use `tezos_smart_rollup_core` directly
pub mod wasm_host {
    pub use tezos_smart_rollup_core::rollup_host::RollupHost as WasmHost;
}
#[deprecated(note = "use tezos_smart_rollup_core directly")]
/// DEPRECATED: use `tezos_smart_rollup_core` directly
pub mod rollup_core {
    pub use crate::{Error, ValueType, METADATA_SIZE};
    pub use tezos_smart_rollup_core::smart_rollup_core::ReadInputMessageInfo;
    pub use tezos_smart_rollup_core::{
        SmartRollupCore as RawRollupCore, MAX_FILE_CHUNK_SIZE, MAX_INPUT_MESSAGE_SIZE,
        MAX_INPUT_SLOT_DATA_CHUNK_SIZE, MAX_OUTPUT_SIZE, MEMORY_INVALID_ACCESS,
        PREIMAGE_HASH_SIZE, STORE_INVALID_ACCESS, STORE_KEY_TOO_LARGE, STORE_NOT_A_VALUE,
        STORE_VALUE_SIZE_EXCEEDED,
    };
}

#[deprecated(note = "use tezos_smart_rollup_core directly")]
pub use tezos_smart_rollup_core::PREIMAGE_HASH_SIZE;

/// The size of a metadata in bytes: 20 (rollup address) + 4 (origination level).
pub use crate::metadata::METADATA_SIZE;

/// Defines the errors possibly returned by an host functions.
#[repr(i32)]
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum Error {
    /// The store key submitted as an argument of a host function exceeds the
    /// authorized limit.
    StoreKeyTooLarge = tezos_smart_rollup_core::STORE_KEY_TOO_LARGE,
    /// The store key submitted as an argument of a host function cannot be
    /// parsed.
    StoreInvalidKey = tezos_smart_rollup_core::STORE_INVALID_KEY,
    /// The contents (if any) of the store under the key submitted as an
    /// argument of a host function is not a value.
    StoreNotAValue = tezos_smart_rollup_core::STORE_NOT_A_VALUE,
    /// An access in a value of the durable storage has failed, supposedly out
    /// of bounds of a value.
    StoreInvalidAccess = tezos_smart_rollup_core::STORE_INVALID_ACCESS,
    /// Writing a value has exceeded 2^31 bytes.
    StoreValueSizeExceeded = tezos_smart_rollup_core::STORE_VALUE_SIZE_EXCEEDED,
    /// An address is out of bound of the memory.
    MemoryInvalidAccess = tezos_smart_rollup_core::MEMORY_INVALID_ACCESS,
    /// The input or output submitted as an argument of a host function exceeds
    /// the authorized limit.
    InputOutputTooLarge = tezos_smart_rollup_core::INPUT_OUTPUT_TOO_LARGE,
    /// Generic error code for unexpected errors.
    GenericInvalidAccess = tezos_smart_rollup_core::GENERIC_INVALID_ACCESS,
    /// A value cannot be modified if it is readonly.
    StoreReadonlyValue = tezos_smart_rollup_core::STORE_READONLY_VALUE,
}

impl From<i32> for Error {
    fn from(code: i32) -> Self {
        match code {
            tezos_smart_rollup_core::STORE_KEY_TOO_LARGE => Self::StoreKeyTooLarge,
            tezos_smart_rollup_core::STORE_INVALID_KEY => Self::StoreInvalidKey,
            tezos_smart_rollup_core::STORE_NOT_A_VALUE => Self::StoreNotAValue,
            tezos_smart_rollup_core::STORE_VALUE_SIZE_EXCEEDED => {
                Self::StoreValueSizeExceeded
            }
            tezos_smart_rollup_core::STORE_INVALID_ACCESS => Self::StoreInvalidAccess,
            tezos_smart_rollup_core::MEMORY_INVALID_ACCESS => Self::MemoryInvalidAccess,
            tezos_smart_rollup_core::INPUT_OUTPUT_TOO_LARGE => Self::InputOutputTooLarge,
            tezos_smart_rollup_core::GENERIC_INVALID_ACCESS => Self::GenericInvalidAccess,
            tezos_smart_rollup_core::STORE_READONLY_VALUE => Self::StoreReadonlyValue,
            _ => Error::GenericInvalidAccess,
        }
    }
}

impl From<i64> for Error {
    fn from(code: i64) -> Self {
        match i32::try_from(code) {
            Ok(error) => error.into(),
            Err(_) => Error::GenericInvalidAccess,
        }
    }
}

impl Error {
    /// Extracts the error from the returned value as a result
    pub fn wrap(code: i32) -> Result<usize, Self> {
        if code >= 0 {
            // Casting to usize is safe, since we eluded the negative values
            Ok(code as usize)
        } else {
            Err(code.into())
        }
    }

    /// Returns the code for the given error.
    pub fn code(self) -> i32 {
        self as i32
    }
}

/// Returned by [`tezos_smart_rollup_core::smart_rollup_core::store_has`] - specifies whether a path has a value, and/or is a prefix.
#[repr(i32)]
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum ValueType {
    /// The path has no matching value or subtree.
    None = tezos_smart_rollup_core::VALUE_TYPE_NONE,
    /// The path has a value, but is not a prefix to further values.
    Value = tezos_smart_rollup_core::VALUE_TYPE_VALUE,
    /// The path is a prefix to further values, but has no value.
    Subtree = tezos_smart_rollup_core::VALUE_TYPE_SUBTREE,
    /// The path has a value, and is a prefix to further values.
    ValueWithSubtree = tezos_smart_rollup_core::VALUE_TYPE_VALUE_WITH_SUBTREE,
}

impl From<ValueType> for i32 {
    fn from(code: ValueType) -> i32 {
        code as i32
    }
}

impl ValueType {
    /// Extracts the error or result from the code as a result.
    pub fn wrap(code: i32) -> Result<Self, Error> {
        match code {
            tezos_smart_rollup_core::VALUE_TYPE_NONE => Ok(Self::None),
            tezos_smart_rollup_core::VALUE_TYPE_VALUE => Ok(Self::Value),
            tezos_smart_rollup_core::VALUE_TYPE_SUBTREE => Ok(Self::Subtree),
            tezos_smart_rollup_core::VALUE_TYPE_VALUE_WITH_SUBTREE => {
                Ok(Self::ValueWithSubtree)
            }
            _ => Err(Error::from(code)),
        }
    }
}
